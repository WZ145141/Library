package com.example.apkcontroller

import android.Manifest
import android.animation.Animator
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Point
import android.os.Build
import android.os.Handler
import android.os.Message
import android.view.*
import android.widget.*
import androidx.core.view.isVisible
import kotlin.math.abs

/**
 *
 * @Description: 作用描述
 * @Author: windowDialog
 * @CreateDate: 2021.3.19  15:15
 * @UpdateUser: 更新者
 * @UpdateDate: 2021.3.19 15:15
 * @UpdateRemark: 更新说明
 * @Version: 1
 */
class windowDialog constructor(context: Context, build: Builder? = null) : View.OnTouchListener {
    companion object {
        private lateinit var mContext: Context
        private const val ANIM_CLOSE = 20
        private var mbuilder: Builder? = null
    }

    init {
        mContext = context

        if (build != null) {
            mbuilder = build
        }
        initWindow()
    }


    class Builder(private val mContext: Context) {

        companion object {
            @Volatile
            private var instance: Builder? = null

            fun getInstance(mContext: Context): Builder =
                instance ?: synchronized(this) {
                    instance
                        ?: Builder(mContext).also {
                            instance = it
                            this.mContext = mContext
                        }
                }

            private var mContext: Context? = null
        }

        @Volatile
        private var instance: windowDialog? = null

        fun getInstance(): windowDialog =
            instance ?: synchronized(this) {
                instance
                    ?: windowDialog(mContext, this).also {
                        instance = it
                    }
            }

        private var isupdata: Boolean = false
        private var title: String = ""
        private var content: String = ""
        private var time: String = ""

        fun isUpdata(isupdata: Boolean): Builder {
            this.isupdata = isupdata
            return this
        }

        val getUpdatastate: Boolean
            get() = this.isupdata


        fun setTitle(title: String): Builder {
            this.title = title
            return this
        }

        val getTile: String
            get() = this.title


        fun setTime(title: String): Builder {
            this.time = title
            return this
        }

        val getTime: String
            get() = this.time


        fun setContent(_content: String): Builder {
            this.content = _content
            return this
        }

        val getContent: String
            get() = this.content


        fun build(): windowDialog {
            return if (isupdata) {
                getInstance()
            } else {
                windowDialog(mContext, this)
            }

        }


    }


    private var downX = 0
    private var downY = 0
    private val mHander: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            if (msg.what === ANIM_CLOSE) {
                animatordismiss()
            }
        }
    }
    /*
    * 1. 初始化显示弹窗
    * 2.显示弹窗  动画
    * 3.定时关闭弹窗
    * 4.关闭弹窗动画
    * */


    /**
     * 显示弹窗
     */
    fun ShowWindow() {
        /**
         * 1.初始化弹窗
         */

        println(vm == null)
        println(layout.parent != null)
        if (vm != null && layout.parent != null && mbuilder?.getUpdatastate == true) {
            mHander.removeMessages(ANIM_CLOSE)
            layout.findViewById<RelativeLayout>(R.id.view_wind)?.let {
                val imgview = it.findViewById<ImageView>(R.id.iv_img)
                val titleview = it.findViewById<TextView>(R.id.tv_title)
                val contentView = it.findViewById<TextView>(R.id.tv_content)
                val timeView = it.findViewById<TextView>(R.id.tv_time)

                timeView?.text = mbuilder?.getTime
                contentView?.text = mbuilder?.getContent
                titleview?.text = mbuilder?.getTile
                layout.updateViewLayout(
                    it, LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                )
            }



            try {
                println("updata")


                vm?.updateViewLayout(layout, vmparams)
                mHander.sendEmptyMessageDelayed(ANIM_CLOSE, 3000)


            } catch (e: Exception) {
                println("updataException")
                show()
            }


        } else {
            vm = mContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager

            println(vm == null)
            vmparams = WindowManager.LayoutParams()
            vmparams?.flags = (WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                    or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    or WindowManager.LayoutParams.FLAG_FULLSCREEN
                    or WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
                vmparams?.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR
            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                vmparams?.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            vmparams?.x = 0
            vmparams?.y = 10
            vmparams?.format = -3  // 会影响Toast中的布局消失的时候父控件和子控件消失的时机不一致，比如设置为-1之后就会不同步
            vmparams?.alpha = 1f

            layout.measure(0, 0)
            vmparams?.height = layout.measuredHeight

            //  vmparams.width = ViewGroup.LayoutParams.MATCH_PARENT

            //  vmparams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            vmparams?.gravity = Gravity.TOP
            println("updata  show")
            show()
        }


    }

    private var vm: WindowManager? = null
    private lateinit var layout: ViewGroup
    var vmparams: WindowManager.LayoutParams? = null
    private var view_window: View? = null

    /**
     *
     * @Description: 初始化window层
     * @Author: 魏征
     * @CreateDate: 2021/3/19 15:47
     * @UpdateUser: 更新者
     * @UpdateDate: 2021/3/19 15:47
     * @UpdateRemark: 更新说明
     * @Params:
     * @Retun: object/any
     */
    private fun initWindow() {
        layout = LinearLayout(mContext)
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        layout.layoutParams = layoutParams
        layout.alpha = 1f



        layout.setOnTouchListener(this)
        val view_window = View.inflate(mContext, R.layout.window_layout, null)

        view_window?.let {
            val imgview = it.findViewById<ImageView>(R.id.iv_img)
            val titleview = it.findViewById<TextView>(R.id.tv_title)
            val contentView = it.findViewById<TextView>(R.id.tv_content)
            val timeView = it.findViewById<TextView>(R.id.tv_time)

            timeView?.text = mbuilder?.getTime
            contentView?.text = mbuilder?.getContent
            titleview?.text = mbuilder?.getTile
        }
        layout.addView(view_window)


        /*       if (vm != null && mbuilder?.getUpdatastate == true) {
                   try {
                       vm?.updateViewLayout(layout, vmparams)
                   } catch (e: Exception) {
                       vm?.addView(layout, vmparams)
                   }
               }else{
                   vm?.addView(layout, vmparams)
               }*/


    }


    private fun show() {
        vm?.addView(layout, vmparams)
        ShowInAnimator(layout)
        // 延迟2s后关闭
        mHander.sendEmptyMessageDelayed(ANIM_CLOSE, 3000)
    }


    /**
     *
     * @Description: 对window 显示添加一个布局
     * @Author: 魏征
     * @CreateDate: 2021/3/19 15:47
     * @UpdateUser: 更新者
     * @UpdateDate: 2021/3/19 15:47
     * @UpdateRemark: 更新说明
     * @Params:
     * @Retun: object/any
     */
    private fun CreateLayout(block: (view: ViewGroup) -> Unit) {

        block(layout)
    }


    /**
     *
     * @Description: 显示进场动画
     * @Author: 魏征
     * @CreateDate: 2021/3/22 9:13
     * @UpdateUser: 更新者
     * @UpdateDate: 2021/3/22 9:13
     * @UpdateRemark: 更新说明
     * @Params:
     * @Retun: object/any
     */
    private fun ShowInAnimator(layout: ViewGroup) {
        val animator = ObjectAnimator.ofFloat(
            layout,
            "translationY",
            -layout.measuredHeight.toFloat(),
            0f
        ) as ObjectAnimator
        animator.duration = 600L
        animator.start()
    }

    /**
     *
     * @Description: dissmiss
     * @Author: 魏征
     * @CreateDate: 2021/3/22 9:33
     * @UpdateUser: 更新者
     * @UpdateDate: 2021/3/22 9:33
     * @UpdateRemark: 更新说明
     * @Params:
     * @Retun: object/any
     */

    private fun dismiss() {
        mHander.removeMessages(ANIM_CLOSE)
        if (!this::layout.isInitialized && null != layout.parent) {
            return
        }
        try {
            vm?.apply {
                this.removeView(layout)
            }
        } catch (e: Exception) {
            //  e.printStackTrace()
        }
    }

    private fun animatordismiss() {
        if (!this::layout.isInitialized && null != layout.parent) {
            return
        }

        val animator = ObjectAnimator.ofFloat(
            layout,
            "translationY",
            layout.translationY,
            -layout.measuredHeight.toFloat()
        )
        animator.duration = 600L
        animator.start()
        animator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {
                dismiss()
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationRepeat(animation: Animator?) {

            }

        }

        )

    }

    private fun animatordismisstoleft() {
        if (!this::layout.isInitialized && null != layout.parent) {
            return
        }

        val animator = ObjectAnimator.ofFloat(
            layout,
            "translationX",
            layout.translationX,
            -layout.measuredWidth.toFloat()
        )
        animator.duration = 600L
        animator.start()
        animator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {
                dismiss()
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationRepeat(animation: Animator?) {

            }

        }

        )

    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event!!.action) {
            MotionEvent.ACTION_DOWN -> {
                mHander.removeMessages(ANIM_CLOSE)
                downY = event.rawY.toInt()
                downX = event.rawX.toInt()
            }
            MotionEvent.ACTION_MOVE -> {

                val moveY = event.rawY
                val moveX = event.rawX
                if (moveY - downY < 10) {//如果是向上滑动  对移动距离做个误差避免无法锁定
                    layout.translationY = moveY - downY

                } else {
                    if (moveX - downX < 10) {//如果是向左滑动
                        layout.translationX = moveX - downX

                    }
                }

            }
            MotionEvent.ACTION_UP -> {
                //达到一定比例后，松开手指将关闭弹窗

                if (layout.translationY < 0) {
                    if (abs(layout.translationY) > layout.measuredHeight / 3) {
                        animatordismiss()
                    } else {
                        layout.translationY = 0f
                    }
                } else {
                    if (abs(layout.translationX) > layout.measuredWidth / 3) {
                        animatordismisstoleft()
                    } else {
                        layout.translationX = 0f
                    }
                }

                mHander.sendEmptyMessageDelayed(ANIM_CLOSE, 3000)
            }
            else -> {
            }
        }
        return true


    }


    /**
     * Android6.0之后需要动态申请权限
     * @param  context Activity
     */
    fun requestPermission(activity: Activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            val permissionsList = ArrayList<String>()
            val permissions = arrayOf(
                Manifest.permission.SYSTEM_ALERT_WINDOW,

            )
            for (perm in permissions) {
                if (PackageManager.PERMISSION_GRANTED != activity.checkSelfPermission(perm)) {
                    permissionsList.add(perm)
                    // 进入到这里代表没有权限.
                }
            }
            if (!permissionsList.isEmpty()) {
                val strings = arrayOfNulls<String>(permissionsList.size)
                activity.requestPermissions(permissionsList.toArray(strings), 0)
            }
        }
    }

}