package com.example.apkcontroller

import android.app.Application
import com.example.baidumaplibrary.BaiduMapManger
import com.tencent.mmkv.MMKV

class APP : Application() {

    companion object {
        lateinit var instance: APP

    }

    override fun onCreate() {
        super.onCreate()
        instance=this
        //MMKV初始化
        val rootDir = MMKV.initialize(this)
       // rootDir.loge("mmkv rootdir:")

        val instances = BaiduMapManger.getInstances()
        instances.mapinit(this)
    }
}