package com.example.apkcontroller

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.baidu.mapapi.map.MapView
import com.baidu.mapapi.model.LatLng
import com.example.baidumaplibrary.BaiduMapManger
import kotlinx.android.synthetic.main.activity_baidu_map.*


class BaiduMapActivity : AppCompatActivity() {
    lateinit var mapView: MapView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_baidu_map)

        BaiduMapManger.getInstances().requestPermission(this)
        mapView = findViewById<MapView>(R.id.map)
        BaiduMapManger.getInstances().OncreateMap(this, mapView)



        add.setOnClickListener {


            val arrayList = arrayListOf<LatLng>()
            arrayList .add(LatLng(
                30.787763718524115,
                103.87009592898018
            ))
            arrayList .add(LatLng(
                30.787182025924274,
                103.87020372564137
            ))
            arrayList .add(LatLng(
                30.78732938838569,
                103.868640674054
            ))

            BaiduMapManger.getInstances().addPolygonFence("",customId = "333",arrayList)

//            BaiduMapManger.getInstances().addRoundFence(
//                50f, "2222", LatLng(
//                    30.788779733073564,
//                    103.87092237004936
//                )
//            )
        }
    }

    /**
     * 方法必须重写
     */
    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    /**
     * 方法必须重写
     */
    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    /**
     * 方法必须重写
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }


    /**
     * 方法必须重写
     */
    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
        try {
            unregisterReceiver(BaiduMapManger.getInstances().mGeoFenceReceiver)
        } catch (ignored: Throwable) {
        }

        BaiduMapManger.getInstances().Destroy()


    }
}