package com.example.apkcontroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class SequenceTestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sequence_test)
        val u = user("weizheng", 1, 1)
        val list = arrayListOf<user>()
        val list2 = arrayListOf<user>()
        val f = (0..100)
        for (i in f) {

            list.add(user("weizheng", i, 1))

            list2.add(user("weizheng", i, 1))

        }
        computeRunTime {
            sequenvoid(list2, list)
        }
        computeRunTime2 {
            forerchvoid(list2, list)
        }


    }

    fun sequenvoid(list2: List<user>, list: List<user>) {
        println(" list2is ${list2.size}")
        println(" listis ${list.size}")
        val list2map = list2.asSequence().map { it.age }.toList()
        println("the code result list2mapsize is ${list2map.size}")
        list.asSequence().filter { it.age in list2map }.forEach {
            println("the code result is ${it}")
        }

        println("the code result size is ${list.size}")
    }

    fun forerchvoid(list2: List<user>, list: List<user>) {
        var i: Int = 0
        list.forEach { arr ->
            list2.let { item ->
                item.find {
                    it.age == arr.age
                }?.let { find ->
                    i++
                    println("the code2 result is $find")
                }
            }
        }
        println("the code2 result size is ${i}")
    }

    fun computeRunTime(action: (() -> Unit)?) {
        val startTime = System.currentTimeMillis()
        action?.invoke()
        println("the code run time is ${System.currentTimeMillis() - startTime}")
    }

    fun computeRunTime2(action: (() -> Unit)?) {
        val startTime = System.currentTimeMillis()
        action?.invoke()
        println("the code2 run time is ${System.currentTimeMillis() - startTime}")
    }
}

data class user(val name: String, var age: Int, val sex: Int)