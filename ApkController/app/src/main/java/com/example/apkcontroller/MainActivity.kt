package com.example.apkcontroller

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.example.baidunavilibrary.BNDemoFactory
import com.example.baidunavilibrary.BNDemoUtils
import com.example.downloadlibrary.download.DownloadInfo
import com.example.downloadlibrary.download.DownloadManager
import com.example.downloadlibrary.download.DownloadResponseHandler
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.*


class MainActivity : AppCompatActivity() {
    val TAG = "MainActivity"
    // 地址A
    val urlA = "http://gdown.baidu.com/data/wisegame/dc429998555b7d4d/QQ_398.apk";
    // 地址B
    val url = "http://track.top1ck.com:6566/AdRedirect.aspx?offerid=1711&affid=2335&cid={cid}";
    private val url1 =
        "https://raw.githubusercontent.com/scwang90/SmartRefreshLayout/master/art/app-debug.apk"
    private val url2 = "https://download.superlearn.com/app/toefl.apk"
    private val url3 = "https://leguimg.qcloud.com/files/LEGUMAC.zip"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DownloadManager.getInstance(this).setMaxRequests(2);
        EventBus.getDefault().register(this);
        donlow.setOnClickListener { v: View? ->

            /*下载*/
            Log.e(TAG, filesDir.absolutePath + "/toefl.apk")


            DownloadManager.getInstance(applicationContext)
                .download(
                    urlA,
                    filesDir.absolutePath + "/toefl.apk",
                    object : DownloadResponseHandler() {
                        override fun onProgress(currentBytes: Long, totalBytes: Long) {

                        }

                        override fun onFinish(download_file: File?) {
                            Log.e(TAG, "下载完成+${download_file!!.absolutePath}")


                        /*    var uri =
                                Uri.fromFile(File("/data/user/0/com.example.apkcontroller/files/toefl.apk"));
                            var localIntent = Intent(Intent.ACTION_VIEW);
                            localIntent.setDataAndType(
                                uri,
                                "application/vnd.android.package-archive"
                            );
                            startActivity(localIntent);*/
                        }

                        override fun onPause(downloadInfo: DownloadInfo?) {
                        }

                        override fun onCancle(downloadInfo: DownloadInfo?) {

                        }

                        override fun onFailure(error_msg: String?) {
                        }

                    })
        }

        installf.setOnClickListener { v: View? ->

            var intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            startActivity(intent);


        }
        install.setOnClickListener { v: View? ->

            /*     *//* 静默安装*//*
            fileinfo?.let {

            }*/
            /*   var isinstall =
                   MyAccessibilityService.installApp("/data/user/0/com.example.apkcontroller/files/toefl.apk")*/
/*            var uri =
                getUriForFile(this, File("/data/user/0/com.example.apkcontroller/files/toefl.apk"));*/
           /* var localIntent = Intent(Intent.ACTION_VIEW);
            localIntent.setDataAndType(uri, "application/vnd.android.package-archive");
            startActivity(localIntent);*/
            installApk(this, File("/data/user/0/com.example.apkcontroller/files/toefl.apk"))
            //  Log.e("install",isinstall.toString())
        }

        baidunavi.setOnClickListener {
            if (BaiduNaviManagerFactory.getBaiduNaviManager().isInited()) {
                BNDemoFactory.getInstance().initRoutePlanNode()
                BNDemoUtils.gotoDriving(this)
            }
        }

    }

    lateinit var fileinfo: DownloadInfo
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(info: DownloadInfo) {
        fileinfo = info

        message.text =
            ("文件名:" + info.fileName) + ("下载地址url:" + info.url) + ("\n" + "存储地址url:" + info.targetUrl) + ("\n" + info.url + "---progress:" + info.progress + "---total:" + info.total) + ("下载状态：" + info.downloadState)
    }

    override fun onDestroy() {
        super.onDestroy()

        EventBus.getDefault().unregister(this);
    }


    companion object {
        //是否有外存
        fun hasExternalStorage(): Boolean {
            return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
        }

        fun chmodFile(file: File) {
            val mode = "704"
            val command = arrayOf("chmod", mode, file.path)
            val builder = ProcessBuilder(*command)
            try {
                builder.start()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        val applicationId = "com.example.apkcontroller"
        val FILE_PROVIDER_AUTH = "${applicationId}.fileProvider"
        //根据文件获取Uri
        fun getUriForFile(context: Context, file: File): Uri? {
            Log.e("WS", "SD卡：" + hasExternalStorage() + ",downloadApk path:" + file)
            var fileUri: Uri?
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {//24 android7
                fileUri = FileProvider.getUriForFile(
                    context,
                    FILE_PROVIDER_AUTH,
                    file
                )
            } else {
                if (!hasExternalStorage())
                    chmodFile(file)//没有SD卡，需要更改文件权限
                //file.setReadable(true,false);
                fileUri = Uri.fromFile(file)
            }
            Log.e("WS", "downloadApk Uri:" + fileUri!!)
            return fileUri
        }


        /**
         * 下载成功后安装apk
         *
         * @param file 安装文件
         */
        fun installApk(context: Context,file: File) {
            //此处file应为apk下载的完整路径文件，这里下载在了外部存储的私有缓存文件夹下，
            // 你可以选择其他存储地址如：外置SD卡等，但是要注意与file_paths.xml中<external-cache-path>，<files-path>......里的
            //path="myApkFile/" 属性共享的文件夹目录对应一致
        /*    val file =
                File(context.externalCacheDir!!.path + File.separator + "myApkFile", "my.apk")*/
            val intent = Intent(Intent.ACTION_VIEW)
            // 由于没有在Activity环境下启动Activity,设置下面的标签
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //判读版本是否在7.0以上
                // 参数1 上下文, 参数2 Provider主机地址 和清单配置文件中保持一致
                // 参数2 = android:authorities="应用包名.fileprovider"属性值
                // 参数3 = 上一步中共享的apk文件
                Log.e("WS", "downloadApk file:" + file!!)
                val apkUri = FileProvider.getUriForFile(context, FILE_PROVIDER_AUTH, file)
                //添加这一句表示对目标应用临时授权该Uri所代表的文件
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                intent.setDataAndType(apkUri, "application/vnd.android.package-archive")
            } else {
                intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive")
            }
            context.startActivity(intent)
        }

    }

}
