package com.example.apkcontroller


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.viewlibrary.BaseFullBottomSheetFragment


import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        requestPermission(this)


        tvdown.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
        tvprint.setOnClickListener {
            startActivity(Intent(this, PrintActivity::class.java))
        }
        tvSequence.setOnClickListener {
            startActivity(Intent(this, SequenceTestActivity::class.java))
        }
        var i = 0
        tvg.setOnClickListener {

            i++
            windowDialog.Builder.getInstance(APP.instance).setContent("hahahha")
                .setTime("ssssssss$i").isUpdata(false).setTitle("mesg").build().ShowWindow()

        }

        baidumap.setOnClickListener {
            startActivity(Intent(this, BaiduMapActivity::class.java))
        }

        bottondialog.setOnClickListener {

            BaseFullBottomSheetFragment().show( supportFragmentManager, "dialog");



        }
    }

    /**
     * Android6.0之后需要动态申请权限
     * @param  context Activity
     */
    fun requestPermission(activity: Activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            val permissionsList = ArrayList<String>()
            val permissions = arrayOf(
                Manifest.permission.SYSTEM_ALERT_WINDOW,

                )
            for (perm in permissions) {
                if (PackageManager.PERMISSION_GRANTED != activity.checkSelfPermission(perm)) {
                    permissionsList.add(perm)
                    // 进入到这里代表没有权限.
                }
            }
            if (permissionsList.isNotEmpty()) {
                val strings = arrayOfNulls<String>(permissionsList.size)
                activity.requestPermissions(permissionsList.toArray(strings), 0)
            }
        }
    }
}
