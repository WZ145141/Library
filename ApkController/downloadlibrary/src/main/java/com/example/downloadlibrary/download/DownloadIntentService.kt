package com.example.downloadlibrary.download

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log

import java.io.File

class DownloadIntentService : IntentService("DownloadIntentService") {

    val TAG = "DownloadIntentService"
    override fun onStart(intent: Intent?, startId: Int) {
        super.onStart(intent, startId)
        Log.e(TAG, "onStart")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
        Log.e(TAG, "onStartCommand")
    }

    override fun bindService(service: Intent?, conn: ServiceConnection, flags: Int): Boolean {
        return super.bindService(service, conn, flags)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return super.onBind(intent)
    }

    override fun onHandleIntent(intent: Intent?) {

        val url = intent!!.extras.getString("url")
        val targeturl = intent!!.extras.getString("targetUrl")
        Log.e("donwload", targeturl)
        DownloadManager.getInstance(applicationContext)
            .download(url, targeturl, object : DownloadResponseHandler() {
                override fun onProgress(currentBytes: Long, totalBytes: Long) {

                }

                override fun onFinish(download_file: File?) {
                    Log.e(TAG, "下载完成+${download_file!!.absolutePath}")

                }

                override fun onPause(downloadInfo: DownloadInfo?) {
                }

                override fun onCancle(downloadInfo: DownloadInfo?) {

                }

                override fun onFailure(error_msg: String?) {
                }

            })

    }

    companion object {
        fun startIntentService(ct: Context, url: String, targetUrl: String) {

            val int = Intent(ct.applicationContext, DownloadIntentService::class.java)
            int.putExtra("url", url)
            int.putExtra("targetUrl", targetUrl)
            ct.startService(int)
        }
    }


}