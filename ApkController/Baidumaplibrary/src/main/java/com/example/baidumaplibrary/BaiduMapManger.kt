package com.example.baidumaplibrary

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Handler
import android.os.Message
import android.text.TextUtils
import android.util.Log
import com.baidu.geofence.GeoFence
import com.baidu.geofence.GeoFenceClient
import com.baidu.geofence.GeoFenceListener
import com.baidu.geofence.model.DPoint
import com.baidu.location.BDAbstractLocationListener
import com.baidu.location.BDLocation
import com.baidu.location.LocationClient
import com.baidu.location.LocationClientOption
import com.baidu.mapapi.CoordType
import com.baidu.mapapi.SDKInitializer
import com.baidu.mapapi.map.*
import com.baidu.mapapi.model.LatLng
import com.baidu.mapapi.model.LatLngBounds
import java.util.*
import kotlin.collections.ArrayList

/**
 *
 * @Description: 地图围栏，定位相关代码，结合封装
 * @Author: BaiduMapManger
 * @CreateDate: 2021.3.31  9:58
 * @UpdateUser: 更新者
 * @UpdateDate: 2021.3.31 9:58
 * @UpdateRemark: 更新说明
 * @Version: 1
 */
class BaiduMapManger : GeoFenceListener {

    companion object {
        var isInt: Boolean = false  //是否初始化

        // 地理围栏的广播action
         const val GEOFENCE_BROADCAST_ACTION = "com.park.huitongyunlian"
        fun getInstances(): BaiduMapManger {
            return mapHolder.instances
        }
    }


    private object mapHolder {

        val instances: BaiduMapManger = BaiduMapManger()


    }


    /**
     * 初始化地图
     * @param 上下文
     */
    fun mapinit(context: Context) {

        // 在使用 SDK 各组间之前初始化 context 信息，传入 ApplicationContext
        // 默认本地个性化地图初始化方法
        SDKInitializer.initialize(context.applicationContext)
        // 自4.3.0起，百度地图SDK所有接口均支持百度坐标和国测局坐标，用此方法设置您使用的坐标类型.
        // 包括BD09LL和GCJ02两种坐标，默认是BD09LL坐标。
        SDKInitializer.setCoordType(CoordType.BD09LL)
        isInt = true
    }


    /**
     * Android6.0之后需要动态申请权限
     * @param  context Activity
     */
    fun requestPermission(activity: Activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            val permissionsList = ArrayList<String>()
            val permissions = arrayOf(
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.WRITE_SETTINGS
            )
            for (perm in permissions) {
                if (PackageManager.PERMISSION_GRANTED != activity.checkSelfPermission(perm)) {
                    permissionsList.add(perm)
                    // 进入到这里代表没有权限.
                }
            }
            if (!permissionsList.isEmpty()) {
                val strings = arrayOfNulls<String>(permissionsList.size)
                activity.requestPermissions(permissionsList.toArray(strings), 0)
            }
        }
    }


    private var fenceClient: GeoFenceClient? = null
    private var markerOption: MarkerOptions? = null
    private var mMapView: MapView? = null
    private var mBdMap: BaiduMap? = null


    /**
     * 用于显示当前的位置
     * 示例中是为了显示当前的位置，在实际使用中，单独的地理围栏可以不使用定位接口
     */
    private var mlocationClient: LocationClient? = null

    /**
     * map初始化
     */
    fun OncreateMap(context: Context, MapView: MapView) {
        this.mMapView = MapView
        // 初始化地理围栏
        fenceClient = GeoFenceClient(context.applicationContext)

        markerOption = MarkerOptions().draggable(true)


        mBdMap = mMapView!!.map
        mBdMap!!.isMyLocationEnabled = true
        mBdMap!!.getUiSettings().isRotateGesturesEnabled = false
        // mBdMap.setOnMapClickListener(this)


        val filter = IntentFilter()
        filter.addAction(GEOFENCE_BROADCAST_ACTION)
        context.registerReceiver(mGeoFenceReceiver, filter)

        /**
         * 创建pendingIntent
         */

        fenceClient!!.createPendingIntent(GEOFENCE_BROADCAST_ACTION)
        // 在即将触发侦听行为时允许开启高精度定位模式(开启gps定位，gps定位结果优先)

        fenceClient!!.isHighAccuracyLoc(true)
        fenceClient!!.setGeoFenceListener(this)
        /**
         * 设置地理围栏的触发行为,默认为进入
         */
        fenceClient!!.setActivateAction(GeoFenceClient.GEOFENCE_IN_OUT_STAYED)




        mlocationClient = LocationClient(context.applicationContext)
        val option = LocationClientOption()
        option.locationMode = LocationClientOption.LocationMode.Hight_Accuracy
        /// 设置是否进行单次定位，单次定位时调用start之后会默认返回一次定位结果
        option.setOnceLocation(false)
        option.isOpenGps = true // 使用gps定位
        option.setCoorType(GeoFenceClient.BD09LL) // 设置坐标类型
        option.setScanSpan(5000)

        // 设置locationClientOption
        mlocationClient!!.locOption = option

        // 注册LocationListener监听器
        val myLocationListener: MyLocationListener =
            MyLocationListener()
        mlocationClient!!.registerLocationListener(myLocationListener)
        mlocationClient!!.start()
    }


    /**
     * 添加多边形围栏
     * @param string
     */
    fun addPolygonFence(string: String, customId: String?, gonPoints: ArrayList<LatLng>) {

//        if (null == polygonPoints || polygonPoints.size < 3) {
//
//            return
//        }

        fenceList.clear()
        fenceClient!!.removeGeoFence()
        if (mBdMap != null) {
            mBdMap!!.clear() // 批量清除地图图层上所有标记
        }
        fenceMap.clear()

        polygonPoints.clear()

        polygonPoints = gonPoints
        val pointList = java.util.ArrayList<DPoint>()
        for (latLng in polygonPoints) {
            pointList.add(DPoint(latLng.latitude, latLng.longitude))
        }

        fenceClient!!.addGeoFence(pointList, GeoFenceClient.BD09LL, customId)
    }


    /**
     * 添加圆形围栏
     */
    fun addRoundFence(radius: Float, customId: String, LatLng: LatLng) {

        fenceList.clear()
        fenceClient!!.removeGeoFence()
        if (mBdMap != null) {
            mBdMap!!.clear() // 批量清除地图图层上所有标记
        }
        fenceMap.clear()

        polygonPoints.clear()


        if (null == fenceClient) {
            return
        }
        centerLatLng = LatLng
        val centerPoint = DPoint(
            LatLng.latitude,
            LatLng.longitude
        )
        fenceClient!!.addGeoFence(centerPoint, GeoFenceClient.BD09LL, radius, customId)
    }


    private inner class MyLocationListener : BDAbstractLocationListener() {
        override fun onReceiveLocation(location: BDLocation) {
            // mapView 销毁后不在处理新接收的位置
            if (mMapView == null) {
                return
            }
            val locData = MyLocationData.Builder()
                .accuracy(location.radius) // 此处设置开发者获取到的方向信息，顺时针0-360
                .direction(location.direction).latitude(location.latitude)
                .longitude(location.longitude).build()
            mBdMap!!.setMyLocationData(locData)
            val builder = MapStatus.Builder()
            builder.zoom(19.0f)
            mBdMap!!.setMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()))
            val ll = LatLng(
                location.latitude,
                location.longitude
            )
            val u = MapStatusUpdateFactory.newLatLng(ll)
            mBdMap!!.animateMapStatus(u)
        }
    }

    private var handler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(@SuppressLint("HandlerLeak") msg: Message) {
            when (msg.what) {
                0 -> {
                    val sb = StringBuffer()
                    sb.append("添加围栏成功  ")
                    val customId = msg.obj as String
                    if (!TextUtils.isEmpty(customId)) {
                        sb.append(" 业务ID: ").append(customId)
                    }
                    Log.i("map", sb.toString())
                    drawFence2Map()
                }
                1 -> {
                    val errorCode = msg.arg1

                    Log.i("map", "添加围栏失败,ErrorCode = $errorCode".toString())
                }
                2 -> {
                    val statusStr = msg.obj as String

                    Log.i("map", statusStr.toString())
                }
                else -> {
                }
            }

        }
    }

    private var fenceList: ArrayList<GeoFence> = ArrayList()
    override fun onGeoFenceCreateFinished(
        geoFenceList: List<GeoFence>?,
        errorCode: Int,
        customId: String?
    ) {
        val msg = Message.obtain()
        if (errorCode == GeoFence.ADDGEOFENCE_SUCCESS) {
            if (geoFenceList != null) {
                fenceList.addAll(geoFenceList)
            }
            msg.obj = customId
            msg.what = 0
        } else {
            msg.arg1 = errorCode
            msg.what = 1
        }
        handler.sendMessage(msg)
    }

    // 记录已经添加成功的围栏
    private val fenceMap = HashMap<String, GeoFence>()
    private var lock = Any()

    // 中心点坐标
    private var centerLatLng: LatLng? = null

    // 多边形围栏的边界点
    private var polygonPoints: ArrayList<LatLng> = java.util.ArrayList()

    private fun drawFence2Map() {
        object : Thread() {
            override fun run() {
                try {
                    synchronized(lock) {
                        if (fenceList.isEmpty()) {
                            return
                        }
                        for (fence in fenceList) {
                            if (fenceMap.containsKey(fence.fenceId)) {
                                continue
                            }
                            drawFence(fence)
                            fenceMap.put(fence.fenceId, fence)
                        }
                    }
                } catch (e: Throwable) {
                }
            }
        }.start()
    }

    // 当前的坐标点集合，主要用于进行地图的可视区域的缩放
    private val boundsBuilder = LatLngBounds.Builder()
    private fun drawFence(fence: GeoFence) {
        when (fence.type) {
            GeoFence.TYPE_ROUND -> drawCircle(fence, false)
            GeoFence.TYPE_BDMAPPOI -> drawCircle(fence, true)
            GeoFence.TYPE_POLYGON -> //			case GeoFence.TYPE_DISTRICT :
                drawPolygon(fence)
            else -> {
            }
        }

        // 设置所有maker显示在当前可视区域地图中
        val bounds: LatLngBounds = boundsBuilder.build()
        val mapStatusUpdate = MapStatusUpdateFactory.newLatLngBounds(
            bounds, 50,
            50, 50, 50
        )
        // 更新地图状态
        mBdMap!!.animateMapStatus(mapStatusUpdate)

    }

    /**
     * 绘制圆
     */
    private fun drawCircle(fence: GeoFence, isPoi: Boolean) {
        val center: LatLng?
        val radius: Int
        if (isPoi) {
            val bdLocation = BDLocation()
            bdLocation.latitude = fence.center.latitude
            bdLocation.longitude = fence.center.longitude
            val tempLocation = LocationClient
                .getBDLocationInCoorType(bdLocation, BDLocation.BDLOCATION_GCJ02_TO_BD09LL)
            center = LatLng(
                tempLocation.latitude,
                tempLocation.longitude
            )
        } else {
            center = centerLatLng
        }
        radius = fence.radius.toInt()
        // 绘制一个圆形
        if (center == null) {
            return
        }
        mBdMap!!.addOverlay(
            CircleOptions().center(center)
                .radius(radius)
                .fillColor(0x666495ED) // 填充颜色
                .stroke(Stroke(3, -0x199b6a13))
        )
        boundsBuilder.include(center)
        if (!isPoi) {
            centerLatLng = null
        }
    }

    /**
     * 绘制Polygon
     */
    private fun drawPolygon(fence: GeoFence) {
        val pointList: List<DPoint>? = fence.points
        if (null == pointList || pointList.isEmpty()) {
            return
        }
        val lst: MutableList<LatLng> = java.util.ArrayList()
        for (point in pointList) {
            lst.add(LatLng(point.latitude, point.longitude))
            boundsBuilder.include(
                LatLng(point.latitude, point.longitude)
            )
        }
        mBdMap!!.addOverlay(
            PolygonOptions()
                .points(polygonPoints)
                .fillColor(0x666495ED) // 填充颜色
                .stroke(Stroke(5, -0x199b6a13))
        )
        if (polygonPoints != null && polygonPoints.size > 0) {
            polygonPoints.clear()
        }
    }

    fun Destroy() {

        fenceClient?.removeGeoFence()

        mlocationClient?.stop()
    }


    /**
     * 接收触发围栏后的广播,当添加围栏成功之后，会立即对所有围栏状态进行一次侦测，只会发送一次围栏和位置之间的初始状态；
     * 当触发围栏之后也会收到广播,对于同一触发行为只会发送一次广播不会重复发送，除非位置和围栏的关系再次发生了改变。
     */
    public val mGeoFenceReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // 接收广播
            if (intent.action == GEOFENCE_BROADCAST_ACTION) {
                val bundle = intent.extras
                Log.e("location:", bundle.toString())
                val customId = bundle!!.getString(GeoFence.BUNDLE_KEY_CUSTOMID)
                val fenceId = bundle!!.getString(GeoFence.BUNDLE_KEY_FENCEID)
                val geoFence: GeoFence = bundle!!.getParcelable(GeoFence.BUNDLE_KEY_FENCE)!!
                val status = bundle.getInt(GeoFence.BUNDLE_KEY_FENCESTATUS)
                val locType = bundle.getInt(GeoFence.BUNDLE_KEY_LOCERRORCODE)
                val sb = StringBuffer()
                when (status) {
                    GeoFence.INIT_STATUS_IN -> sb.append("围栏初始状态:在围栏内")
                    GeoFence.INIT_STATUS_OUT -> sb.append("围栏初始状态:在围栏外")
                    GeoFence.STATUS_LOCFAIL -> sb.append("定位失败,无法判定目标当前位置和围栏之间的状态")
                    GeoFence.STATUS_IN -> sb.append("进入围栏 ")
                    GeoFence.STATUS_OUT -> sb.append("离开围栏 ")
                    GeoFence.STATUS_STAYED -> sb.append("在围栏内停留超过10分钟 ")
                    else -> {
                    }
                }
                if (status != GeoFence.STATUS_LOCFAIL) {
                    if (!TextUtils.isEmpty(customId)) {
                        sb.append(" 业务Id: $customId")
                    }
                    sb.append(" 地理围栏ID: $fenceId")
                }
                val str = sb.toString()
                val msg = Message.obtain()
                msg.obj = str
                msg.what = 2
                handler.sendMessage(msg)
            }
        }
    }


}