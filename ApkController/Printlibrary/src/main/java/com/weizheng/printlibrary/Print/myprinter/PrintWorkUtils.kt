package com.weizheng.printlibrary.Print.myprinter

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.os.Build
import android.os.Handler
import android.os.Message
import android.util.Log
import com.lvrenyang.io.*
import com.weizheng.printlibrary.Print.*

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.io.UnsupportedEncodingException

class PrintWorkUtils : BasePrintAdapter {


    var HaveQR: Boolean = true
    private var isConnecting = false
    private var bt: BTPrinting? = null
    private var ble: BLEPrinting? = null
    private var net: NETPrinting? = null
    private var usb: USBPrinting? = null
    private val pos = Pos()

    /**
     * 连接打印机,默认连接失败
     */
    @SuppressLint("ObsoleteSdkInt")
    fun connect(address: String): Observable<States> {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            usb = USBPrinting()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            ble = BLEPrinting()
        }
        bt = BTPrinting()
        net = NETPrinting()

        var status: States? = null
        connectBt(address) {
            status = States(msg = "连接错误", code = it, isSuccess = false)
        }
        return Observable.create<States> {

            it.onNext(status!!)
            it.onComplete()
        }
    }


    /**
     * 打印文本 默认utf-8 打印
     */
    fun PrlintTeXT(str: String, strQrcode: String? = null): Observable<States> {

        if (str.isBlank()) {
            return Observable.create<States> {
                it.onNext(States("打印文本空", isSuccess = false, code = -1))
                it.onComplete()
            }
        }
        // 设置UTF8编码
        // Android手机默认也是UTF8编码
        // 设置UTF8编码
        // Android手机默认也是UTF8编码


        val header = byteArrayOf(0x1b, 0x40, 0x1c, 0x26, 0x1b, 0x39,
                0x01)

        var strbuf: ByteArray? = null
        try {
            strbuf = str.toByteArray(charset("UTF-8"))
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        val buffer = DataUtils.byteArraysToBytes(arrayOf(
                header,
                strbuf
        ))

        val result :Boolean = if (USBPrinting::class.java.name ==
                pos.IO.javaClass.name) true else pos.POS_QueryOnline(1000)
        val offset = 0
        val count = buffer.size
        val code: Int
        val status: States
        if (result) {

            if (pos.IO.Write(buffer, offset, count) == count) {
                code = 1
                status = States(code = code, msg = "打印成功", isSuccess = result)
            } else {
                code = 0
                status = States(code = code, msg = "打印错误", isSuccess = result)
            }
        } else {
            code = 0
            status = States(code = code, msg = "打印失败", isSuccess = result)
        }



        return if (strQrcode != null) {
            PrintQRcode(strQrcode = strQrcode)
        } else {
            Observable.create<States> {
                it.onNext(status)
                it.onComplete()
            }
        }
    }

    /**
     * 打印文本 默认utf-8 打印
     */
    fun PrlintTeXT(strlist: ArrayList<PrintModel>, strQrcode: String? = null): Observable<States> {

        if (strlist.isEmpty()) {
            return Observable.create<States> {
                it.onNext(States("打印文本空", isSuccess = false, code = -1))
                it.onComplete()
            }
        }
        var str = ""
        var Qrcode = strQrcode
        strlist.forEach {
            if (it.type == 1) {
                if (str == "") {
                    str += "${it.content}\n\n"
                } else {
                    str += "${it.content}\n"
                }

            } else if (it.type == 2) {
                Qrcode = it.content
                Log.i("Print", "Qrcode:$Qrcode")
                return@forEach
            }
        }

        Log.i("Print", str)
        if (str.isEmpty()) {
            return Observable.create<States> {
                it.onNext(States("打印文本空", isSuccess = false, code = -1))
                it.onComplete()
            }
        }

        // 设置UTF8编码
        // Android手机默认也是UTF8编码
        // 设置UTF8编码
        // Android手机默认也是UTF8编码


        val header = byteArrayOf(0x1b, 0x40, 0x1c, 0x26, 0x1b, 0x39,
                0x01)

        var strbuf: ByteArray? = null
        try {
            strbuf = str.toByteArray(charset("UTF-8"))
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        val buffer = DataUtils.byteArraysToBytes(arrayOf(
                header,
                strbuf
        ))

        val result :Boolean = if (USBPrinting::class.java.name ==
                pos.IO.javaClass.name) true else pos.POS_QueryOnline(1000)
        val offset = 0
        val count = buffer.size
        val code :Int
        val status: States
        if (result) {

            if (pos.IO.Write(buffer, offset, count) == count) {
                code = 1
                status = States(code = code, msg = "打印成功", isSuccess = result)
            } else {
                code = 0
                status = States(code = code, msg = "打印错误", isSuccess = result)
            }
        } else {
            code = 0
            status = States(code = code, msg = "打印失败", isSuccess = result)
        }



        return if (Qrcode != null) {
            PrintQRcode(strQrcode = Qrcode!!)
        } else {
            Observable.create<States> {
                it.onNext(status)
                it.onComplete()
            }
        }
    }

    fun PrintQRcode(strQrcode: String): Observable<States> {
        val nQrcodeWidth: Int = 4
        val nErrorCorrectionLevel = 4
        val nWidthX: Int = nQrcodeWidth
        val necl: Int = nErrorCorrectionLevel
        val nVersion = 5

        val result :Boolean = if (USBPrinting::class.java.name ==
                pos.IO.javaClass.name) true else pos.POS_QueryOnline(1000)

        val status: States
        if (result) {

            pos.POS_S_SetQRcode(strQrcode, nWidthX, nVersion, necl)
            status = States(code = 1, msg = "打印成功", isSuccess = result)
            return PrlintTeXT("扫码支付或呼叫收费员")
        } else {
            status = States(code = 0, msg = "打印失败", isSuccess = result)
            return Observable.create<States> {
                it.onNext(status)
                it.onComplete()
            }
        }


        /* Observable.create<States> {
               it.onNext(status)
               it.onComplete()
           }*/
    }

    fun disconnect() {
        disconnectBt()
        disconnectBle()
        disconnectNet()
        disconnectUsb()

    }


    fun disconnectBt() {
        try {
            bt!!.Close()
            connectLister?.OnConnetState(ConnectState.FAILURE, "断开连接！")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun disconnectBle() {
        try {
            ble!!.Close()
            connectLister?.OnConnetState(ConnectState.FAILURE, "断开连接！")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun disconnectNet() {
        try {
            net!!.Close()
            connectLister?.OnConnetState(ConnectState.FAILURE, "断开连接！")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun disconnectUsb() {
        try {
            usb!!.Close()
            connectLister?.OnConnetState(ConnectState.FAILURE, "断开连接！")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun connectBle(BTAddress: String?, callback: (isconnect: Int) -> Unit) {

        isConnecting = true
        pos.Set(ble)
        val result = ble!!.Open(BTAddress)

        isConnecting = false
        callback(if (result) 1 else 0)

    }

    fun connectBt(BTAddress: String?, callback: (isconnect: Int) -> Unit) {
        Log.i("Print", "开始连接")
        isConnecting = true
        pos.Set(bt)

        val result = bt!!.Open(BTAddress)

        isConnecting = false
        callback(if (result) 1 else 0)

    }

    fun connectNet(IPAddress: String?, PortNumber: Int, callback: (isconnect: Int) -> Unit) {

        isConnecting = true
        pos.Set(net)

        val result = net!!.Open(IPAddress, PortNumber)

        isConnecting = false
        callback(if (result) 1 else 0)

    }


    fun connectUsb(manager: UsbManager?, device: UsbDevice?, callback: (isconnect: Int) -> Unit) {

        isConnecting = true
        pos.Set(usb)

        val result = usb!!.Open(manager, device)

        isConnecting = false
        callback(if (result) 1 else 0)


    }

    fun isConnecting(): Boolean {
        return isConnecting
    }

    fun isConnected(): Boolean {

        Log.e("Print", (bt != null || net != null || usb != null || ble != null).toString())


        return when (bt != null || net != null || usb != null || ble != null) {
            true -> {
                bt!!.IsOpened() || net!!.IsOpened() || usb!!.IsOpened() || ble!!.IsOpened()
            }
            false -> false
        }
    }


    internal class MHandler : Handler() {

        override fun handleMessage(msg: Message) {
            val result = msg.arg1
            when (msg.what) {

                Global.MSG_WORKTHREAD_SEND_CONNECTBTRESULT -> {

                    if (result == 1) {
                        /* 蓝牙连接成功  去打印*/
                    }

                }
                Global.CMD_POS_SETQRCODERESULT,
                Global.CMD_EPSON_SETQRCODERESULT -> {


                    if (result == 1) {
                        /* 二维码打印成功*/

                    }

                }

                Global.CMD_POS_WRITERESULT -> {


                    if (result == 1) {
                        /* 文字打印成功*/
                    }
                }
            }
        }

    }

    data class States(val msg: String,
                      val code: Int,
                      val isSuccess: Boolean)


    var connectLister: OnPrintconnetListener? = null
    var connectstateListener: OnPrintingStateListener? = null

    private val compositeDisposable = CompositeDisposable()
    override fun initprinter(context: Context) {

    }

    override fun connet(address: String): Boolean {
        compositeDisposable.add(connect(address)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    connectLister?.OnConnetState(ConnectState.START)
                }
                .doOnComplete {

                }
                .doOnError {
                    connectLister?.OnConnetState(ConnectState.UNKNOWN, it.message)
                }
                .subscribe {
                    when (it.code) {
                        0 -> {
                            connectLister?.OnConnetState(ConnectState.FAILURE, it.msg)
                        }
                        1 -> {
                            connectLister?.OnConnetState(ConnectState.SUCCESS, it.msg)
                        }
                    }

                    if (!compositeDisposable.isDisposed) {
                        compositeDisposable.dispose()
                    }
                }
        )
        return false
    }

    override fun disconnet(): Boolean {

        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
        disconnect()
        return false
    }

    override fun PrintData(data: ArrayList<PrintModel>) {
        compositeDisposable
                .add(PrlintTeXT(data)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe {
                            connectstateListener?.doStart()
                        }
                        .doOnComplete {
                            connectstateListener?.doOnComplete()
                            if (!compositeDisposable.isDisposed) {
                                compositeDisposable.dispose()
                            }
                        }
                        .doOnError {
                            connectstateListener?.doFailrue()
                            if (!compositeDisposable.isDisposed) {
                                compositeDisposable.dispose()
                            }
                        }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({


                        }, {
                            connectstateListener?.doOnError(it.message.toString())
                        }))
    }

    override fun addConnectListener(listener: OnPrintconnetListener) {
        connectLister = listener
    }

    override fun addConnectingListener(listener: OnPrintingStateListener) {
        connectstateListener = listener
    }


}


