package com.weizheng.printlibrary.Print


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.haoge.easyandroid.easy.EasyActivityResult
import com.haoge.easyandroid.easy.EasyPermissions
import com.haoge.easyandroid.easy.EasyToast
import com.kongzue.dialog.v3.FullScreenDialog
import com.weizheng.printlibrary.Print.myprinter.PrintWorkUtils
import com.weizheng.printlibrary.Print.myprinter.V2PrintUtil
import com.weizheng.printlibrary.R
import com.weizheng.printlibrary.bean.CarRecording
import com.weizheng.printlibrary.bean.CheckCostRecord
import com.weizheng.printlibrary.bean.CheckInOutResponseModel
import com.weizheng.printlibrary.bean.DayReportRes
import com.weizheng.printlibrary.utils.DateUtils
import com.weizheng.printlibrary.utils.MMKVuTil
import com.weizheng.printlibrary.utils.RecycleViewDivider
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList


class PrintMeanger private constructor() : OnPrintingStateListener, OnPrintconnetListener {


    private object PrintHelper {

        val instances: PrintMeanger = PrintMeanger()
    }

    var isConnected: Boolean = false // 是否连接成功

    var blueopen: Boolean = false // 蓝牙是否连接成功

    companion object {
        fun getInstance(): PrintMeanger {
            return PrintHelper.instances
        }

        const val BIND_DEVICE = "BLUE_DEVICE"
        const val PRINT_STATE = "PRINT_STATE"
    }

    private var currentActivity: Activity? = null
    private var cs2adapter: BasePrintAdapter? = null  //cs2/cc2
    private var Mptadapter: BasePrintAdapter? = null  //Mpt
    private var Escadapter: BasePrintAdapter? = null  //esc
    private var v2Printadapter: BasePrintAdapter? = null //商米

    var listener: connectListener? = null
    private fun removeListener() {
        listener = null
    }

    fun setcurrentActivity(activity: Activity?) {
        currentActivity = activity
    }


    private fun showLoadingDialog() {

//        try {
//            currentActivity?.runOnUiThread {
//                if (currentActivity is BaseVMActivity<*>) {
//                    (currentActivity as BaseVMActivity<*>).showLoadingDialog()
//                }
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }

    }

    private fun hideLoadingDialog() {

//        try {
//            currentActivity?.runOnUiThread {
//                if (currentActivity is BaseVMActivity<*>) {
//                    (currentActivity as BaseVMActivity<*>).hideLoadingDialog()
//                }
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }

    }


    init {
        when (Build.DEVICE) {
            "S60" -> {

            }
            "V2" -> {
                v2Printadapter = V2PrintUtil.getInstance()
            }
            else -> {
                try {
                    cs2adapter = PrintUtil2.getInstance()
                } catch (e: Exception) {
                }
                try {
                    Mptadapter = PrintWorkUtils()

                } catch (e: Exception) {
                }
            }
        }


    }

    /**
     * esc 初始化
     */
    fun escinit() {
        if (Build.DEVICE == "S60") {
            try {
                Escadapter = EscPrintUtil.getInstance()
                (Escadapter as EscPrintUtil).initvoid()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /**
     * esc 初始化
     */
    fun PrintUtil2init(context: Context) {
        when (Build.DEVICE) {
            "S60" -> {

            }
            "V2" -> {

            }
            else -> {
                try {
                    cs2adapter = PrintUtil2.getInstance()
                    (cs2adapter as PrintUtil2).initprinter(context)
                } catch (e: Exception) {
                }
                try {
                    Mptadapter = PrintWorkUtils()

                } catch (e: Exception) {
                }
            }
        }
    }

    /**
     * @description : 蓝牙适配器设置
     * @creation date: 2020/7/10  10:32
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun PrintSytem(context: AppCompatActivity? = null, listener: connectListener? = null) {

        if (Build.DEVICE == "S60") {
            return
        }
        if (Build.DEVICE == "V2") {
            return
        }
        var mContext = currentActivity as AppCompatActivity
        if (context != null) {
            mContext = context
        }
        showSysDialog(mContext, listener)

    }

    /**
     * @description : ；蓝牙设配器列表
     * @creation date: 2020/7/10  10:31
     * @User user:Administrator
     * @param :
     * @Return :
     */
    private fun showSysDialog(context: AppCompatActivity?, listener: connectListener? = null) {

        context?.let {
            //全屏对话框-打印设置
            FullScreenDialog
                .show(it, R.layout.activity_system_setting) { dialog, rootView ->
                    val recycleview = rootView.findViewById<RecyclerView>(R.id.recylcview)
                    //初始化recycleView
                    val layoutManager = LinearLayoutManager(context)
                    layoutManager.orientation = LinearLayoutManager.VERTICAL
                    recycleview.layoutManager = layoutManager
                    val device = MMKVuTil().mmkv.decodeParcelable(
                        BIND_DEVICE,
                        Bluedevice::class.java
                    )
                    val buleAdapter =
                        prlintAdapter(ArrayList(), device, listener, dialog)
                    val list = checPermissions(context, dialog = dialog, adapter = buleAdapter)
                    recycleview.adapter = buleAdapter
                    buleAdapter.setNewData(list)
                    recycleview.addItemDecoration(
                        RecycleViewDivider(
                            it,
                            LinearLayoutManager.HORIZONTAL
                        )
                    )
                    rootView.findViewById<ProgressBar>(R.id.pb_search_loading).visibility =
                        View.GONE
                    rootView.findViewById<TextView>(R.id.tv_blue_status).visibility = View.GONE

                }
                .setOnDismissListener {
                    // it.unregisterReceiver(receiver)
                }.setOnShowListener {
                    // context.registerReceiver(receiver, makeFilter());
                }
                .setCancelButton("取消").title = "打印设置"
        }

    }

    var receiver = BluetoothListenerReceiver()
    private fun makeFilter(): IntentFilter {
        val filter = IntentFilter()
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED)
        return filter
    }

    /**
     * @description : 权限检查,获取设配器列表
     * @creation date: 2020/7/10  10:30
     * @User user:Administrator
     * @param :
     * @Return :
     */
    private fun checPermissions(
        context: AppCompatActivity?,
        dialog: FullScreenDialog,
        adapter: prlintAdapter
    ): ArrayList<BluetoothDevice> {
        val deviceLists = ArrayList<BluetoothDevice>()
        context?.let {
            EasyPermissions.create(
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN
            )
                .callback { grant ->
                    if (grant) {
                        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                        if (bluetoothAdapter == null) {

                            EasyToast.DEFAULT.show("没有找到蓝牙适配器")
                            return@callback
                        }
                        if (!bluetoothAdapter.isEnabled) {
                            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)

                            EasyActivityResult.startActivity(
                                it,
                                enableBtIntent
                            ) { resultCode: Int, intent: Intent? ->

                                if (resultCode == Activity.RESULT_OK) {

                                    val pairedDevices = bluetoothAdapter.bondedDevices

                                    deviceLists.clear()
                                    deviceLists.addAll(pairedDevices)
                                    adapter.setNewData(deviceLists)
                                }
                            }
                        }

                        val pairedDevices = bluetoothAdapter.bondedDevices

                        deviceLists.clear()
                        deviceLists.addAll(pairedDevices)

                    } else {

                        dialog.doDismiss()
                        EasyToast.DEFAULT.show("请授予权限")
                    }
                }.request(it)
            return deviceLists
        }

        return deviceLists
    }


    /**
     * @description :已选址过地址 尝试连接地址
     *  @return true 连接成功 flase连接失败
     * @creation date: 2020/7/10  10:33
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun printconnet(listener: connectListener): Boolean {
        val device = MMKVuTil().mmkv.decodeParcelable(BIND_DEVICE, Bluedevice::class.java)

        Log.e("device:", "${device != null && device.address.isNotEmpty()}")
        Log.e("Build.DEVICE:", Build.DEVICE)
        if (device != null && device.address.isNotEmpty()) {
            return when (Build.DEVICE) {
                "S60" -> {
                    this.listener = listener
                    Escadapter!!.addConnectListener(this)
                    Escadapter!!.addConnectingListener(this)
                    Escadapter?.connet("s60")

                    true
                }
                "V2" -> {
                    this.listener = listener
                    v2Printadapter!!.addConnectListener(this)
                    v2Printadapter!!.addConnectingListener(this)
                    v2Printadapter?.connet("V2")
                    true
                }
                else -> {
                    this.listener = listener
                    connetDevice(device)
                    true
                }
            }

        } else {
            return when (Build.DEVICE) {
                "S60" -> {
                    this.listener = listener
                    Escadapter!!.addConnectListener(this)
                    Escadapter!!.addConnectingListener(this)
                    Escadapter?.connet("s60")

                    true
                }
                "V2" -> {
                    this.listener = listener
                    v2Printadapter!!.addConnectListener(this)
                    v2Printadapter!!.addConnectingListener(this)
                    v2Printadapter?.connet("V2")

                    true
                }
                else -> {

                    PrintSytem(listener = listener)
                    false
                }
            }


        }

    }


    /**
     * @description : 连接 蓝牙适配器地址
     * @creation date: 2020/7/10  10:33
     * @User user:Administrator
     * @param :
     * @Return :
     */
    private fun connetDevice(device: Bluedevice) {
        if (device.name.contains("MPT")) {
            Mptadapter!!.connet(device.address)
        } else if (device.name.contains("CS2") || device.name.contains("CC2")) {
            cs2adapter!!.addConnectListener(this)
            cs2adapter!!.addConnectingListener(this)
            cs2adapter!!.connet(device.address)
        }
    }

    /**
     *  已选址过地址 断开连接地址
     *  @return true 断开成功 flase断开失败
     */
    fun printdisconnet(): Boolean {
        val device = MMKVuTil().mmkv.decodeParcelable(BIND_DEVICE, Bluedevice::class.java)
        if (device != null && device.address.isNotEmpty()) {
            if (device.name.contains("MPT")) {
                Mptadapter!!.disconnet()
            } else if (device.name.contains("CS2") || device.name.contains("CC2")) {
                cs2adapter!!.disconnet()

            }
            return true
        }
        return false
    }


    /**
     * @description :打印 标准 数据
     * @creation date: 2020/7/10  10:34
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun PrintData(list: ArrayList<PrintModel>) {

        Log.i("printlibr", "判断是否连接")
        if (isConnected) {
            val bluetoothDevice =
                MMKVuTil().mmkv.decodeParcelable(BIND_DEVICE, Bluedevice::class.java)
            when (bluetoothDevice != null) {
                true -> {
                    if (bluetoothDevice.name.contains("MPT")) {

                        Mptadapter!!.PrintData(list)
                    } else if (bluetoothDevice.name.contains("CS2") || bluetoothDevice.name.contains(
                            "CC2"
                        )
                    ) {
                        cs2adapter!!.PrintData(data = list)
                    }
                }

                false -> {
                    when (Build.DEVICE) {
                        "S60" -> Escadapter?.PrintData(list)
                        "V2" -> {
                            v2Printadapter?.PrintData(list)

                        }
                        else -> {

                        }
                    }

                }
            }
        } else {
            when (Build.DEVICE) {
                "S60" -> Escadapter?.PrintData(list)
                "V2" -> {
                    v2Printadapter?.PrintData(list)

                }
                else -> {

                }
            }
        }
    }


    inner class prlintAdapter(
        dataLists: ArrayList<BluetoothDevice>,
        address: Bluedevice?,
        listener: connectListener? = null,
        dialog: FullScreenDialog
    ) : BaseQuickAdapter<BluetoothDevice, BaseViewHolder>(R.layout.item, dataLists) {
        private var _address = address
        private val _listener = listener
        private val _dialog = dialog

        @SuppressLint("ResourceAsColor")
        override fun convert(helper: BaseViewHolder, item: BluetoothDevice?) {
            item?.let {

                helper.setText(R.id.text, "设备名称:${item.name}\n设备地址:${item.address}")
                val tv = helper.getView<TextView>(R.id.text)
                tv.setTextColor(R.color.tipTextColor)
                _address = MMKVuTil().mmkv.decodeParcelable(
                    BIND_DEVICE,
                    Bluedevice::class.java
                )
                if (_address != null) {
                    if (_address!!.address == item.address) {
                        tv.setTextColor(R.color.holo_red_light)
                    } else {
                        tv.setTextColor(R.color.tipTextColor)
                    }
                }

                helper.itemView.setOnClickListener {

                    printdisconnet()
                    if (isConnected) {
                        if (_address!!.address != item.address) {

                            MMKVuTil().mmkv.encode(
                                BIND_DEVICE,
                                Bluedevice(item.address, item.name)
                            )
                            tv.setTextColor(R.color.holo_red_light)
                            if (_listener != null) {
                                getInstance().listener = _listener

                            } else {
                                //  setconnectLister(_dialog)
                            }
                            connetDevice(Bluedevice(item.address, item.name))
                            _dialog.doDismiss()

                        }

                    } else {
                        MMKVuTil().mmkv.encode(
                            BIND_DEVICE,
                            Bluedevice(item.address, item.name)
                        )
                        tv.setTextColor(R.color.holo_red_light)
                        if (_listener != null) {
                            getInstance().listener = _listener

                        } else {
                            // setconnectLister(_dialog)
                        }
                        connetDevice(Bluedevice(item.address, item.name))
                        _dialog.doDismiss()
                    }


                }
            }
        }

    }

    /**
     * @description : 设置打印监听
     * @creation date: 2020/7/10  10:34
     * @User user:Administrator
     * @param :
     * @Return :
     */
    private fun setconnectLister(_dialog: FullScreenDialog) {
        printconnet(object : connectListener {

            override fun connect(state: ConnectState, error: String?) {
                when (state) {
                    ConnectState.START -> {

                    }
                    ConnectState.FAILURE -> {

                        EasyToast.DEFAULT.show("连接失败")
                    }
                    ConnectState.SUCCESS -> {
                        EasyToast.DEFAULT.show("连接成功")
                        _dialog.doDismiss()

                    }
                    ConnectState.DRIVCE_HOT -> {

                    }
                    ConnectState.NO_PAGER -> {

                    }
                    ConnectState.NO_RESPONSE -> {

                    }
                    else -> {

                    }
                }
            }

        })
    }

    inner class BluetoothListenerReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent!!.action) {
                BluetoothAdapter.ACTION_STATE_CHANGED -> {
                    when (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0)) {
                        BluetoothAdapter.STATE_ON -> {
                            blueopen = true
                        }

                        BluetoothAdapter.STATE_OFF -> {
                            blueopen = false
                        }
                    }
                }
            }
        }
    }


    override fun doOnComplete() {
        hideLoadingDialog()

        listener?.doOnComplete()
        removeListener()
    }

    override fun doOnError(error: String) {
        hideLoadingDialog()
        listener?.doOnError(error)
        removeListener()

    }

    override fun doStart() {
        showLoadingDialog()
        EasyToast.DEFAULT.show("打印中...")
        listener?.doStart()

    }

    override fun doFailrue() {
        hideLoadingDialog()
        listener?.doFailrue()
        removeListener()
    }


    override fun OnConnetState(state: ConnectState, error: String?) {

        when (state) {
            ConnectState.START -> {

                showLoadingDialog()
                listener?.connect(state, error)
            }
            ConnectState.FAILURE -> {
                isConnected = false
                if (listener == null)
                //  EasyToast.DEFAULT.show("连接断开")
                    Toast.makeText(currentActivity, "连接断开", Toast.LENGTH_LONG).show()
                removeListener()
                hideLoadingDialog()
                listener?.connect(state, error)
            }
            ConnectState.SUCCESS -> {
                isConnected = true
                if (listener == null)
                //   EasyToast.DEFAULT.show("连接成功")
                    Toast.makeText(currentActivity, "连接成功", Toast.LENGTH_LONG).show()
                hideLoadingDialog()
                listener?.connect(state, error)
            }
            ConnectState.DRIVCE_HOT -> {
                hideLoadingDialog()
                removeListener()

            }
            ConnectState.NO_PAGER -> {
                hideLoadingDialog()
                removeListener()
            }
//            ConnectState.NO_RESPONSE -> {
//                hideLoadingDialog()
//                removeListener()
//            }
            //打印机无法响应
            ConnectState.NO_RESPONSE -> {
                Log.e("printlibr", "打印机无响应")

                isConnected = false
                if (listener == null)
                //    EasyToast.DEFAULT.show("连接断开")
                    Toast.makeText(currentActivity, "打印机无响应", Toast.LENGTH_LONG).show()
                hideLoadingDialog()

                PrintSytem(listener = listener)

            }
            else -> {
                hideLoadingDialog()
                removeListener()
            }
        }


    }

    /**
     * @description : 进车场票据打印   彭山小票打印模式
     * @creation date: 2020/7/10  10:29
     * @User user:Administrator
     * @param :
     * @Return :
     */

    fun printInfoIn(checkInOutResponseModel: CheckInOutResponseModel) {

        val s = MMKVuTil().mmkv.decodeInt(PRINT_STATE, 1)
        Log.i("printlibr", "打印方案$s")
        when (s) {
            0 -> {
                Printin_Z(checkInOutResponseModel)
            }
            1 -> {
                Printin_S(checkInOutResponseModel)
            }
            else -> {
                return
            }
        }


    }

    /**
     * @description : 出车票据打印
     * @creation date: 2020/7/10  10:29
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun printInfoIn(
        checkInOutResponseModel: CheckInOutResponseModel,
        boolean: Boolean,
        REFOUD_OUT: Boolean = false
    ) {

        when (MMKVuTil().mmkv.decodeInt(PRINT_STATE, 1)) {
            0 -> {
                Printout_Z(checkInOutResponseModel, REFOUD_OUT)
            }
            1 -> {
                Printout_S(checkInOutResponseModel, REFOUD_OUT)
            }
            else -> {
                return
            }
        }
    }

    /**
     * @description : 欠费追缴 分类
     * @creation date: 2020/7/10  10:29
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun printConst(
        data: CheckCostRecord? = null,
        type: Int,
        data2: CarRecording? = null,
        totalFee: Float
    ) {

        when (MMKVuTil().mmkv.decodeInt(PRINT_STATE, 1)) {
            0 -> {
                printConst_Z(data, type, data2, totalFee)
            }
            1 -> {
                printConst_S(data, type, data2, totalFee)
            }
            else -> {
                return
            }
        }
    }


    /**
     * @description : 补打欠费追缴 分类
     * @creation date: 2020/7/10  10:29
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun printConstBill(data: CheckCostRecord) {

        when (MMKVuTil().mmkv.decodeInt(PRINT_STATE, 1)) {
            0 -> {
                printConstBill_Z(data)
            }
            1 -> {
                printConstBill_S(data)
            }
            else -> {
                return
            }
        }
    }

    /**
     * @name  注解  今日报表
     * @class name:PrintMeanger.kt
     * @anthor user: Administrator
     * @time 2020/7/9 12:08
     * @change
     * @chang 12:08
     * @version 1.0
     */
    fun printInfoIn(data: DayReportRes) {

        when (MMKVuTil().mmkv.decodeInt(PRINT_STATE, 1)) {
            0 -> {
                printInfoIn_Z(data)
            }
            1 -> {
                printInfoIn_S(data)
            }
            else -> {
                return
            }
        }
    }


    /** <--------------------------------------------------彭 山 精 简 方 案----------------------------------------------------->**/


    /**
     * @description : 彭山进车小票
     * @creation date: 2020/9/21  9:36
     * @User user:Administrator
     * @param :
     * @Return :
     */
    private fun Printin_S(checkInOutResponseModel: CheckInOutResponseModel) {
        val wordSize = 24
        val list = ArrayList<PrintModel>()
        checkInOutResponseModel.apply {

            var model = PrintModel(1, "***********入场凭证***********", 1, 24, 20)
            model.isChangeLine = false
            list.add(model)

            model = PrintModel(1, "操作    :$state", 1, wordSize, 10)
            list.add(model)

            model = PrintModel(
                1,
                "管理单位:$company_name",
                1,
                wordSize,
                5
            )
            list.add(model)




            model = PrintModel(1, "泊位号:$lotCode  车牌:$plateNumber ", 1, wordSize, 5)
            model.isChangeLine = false
            list.add(model)



            model = PrintModel(1, "停车站点:${siteName}", 1, wordSize, 5)
            list.add(model)


            model = PrintModel(1, "入场时间:$inTime", 1, wordSize, 0)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "打印时间:$printtime", 1, wordSize, 0)
            model.isChangeLine = false
            list.add(model)




            model = PrintModel(
                3,
                "· · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · ·",
                1,
                wordSize,
                0,
                bold = 1
            )
            list.add(model)


            if (arrreocept != null && "" != arrreocept) {
                model = PrintModel(
                    2, arrreocept + "$id",
                    2, 14, 2
                )
                list.add(model)
            }
            model = PrintModel(
                3,
                "",
                1,
                wordSize,
                0,
                bold = 0
            )
            model = PrintModel(
                1, "扫码缴费,自主离场",
                2, 30, 5, bold = 1
            )
            list.add(model)
            model = PrintModel(1, "收费员联系电话:$inManPhoneNo", 1, 24, 5, bold = 0)
            model.isChangeLine = false
            list.add(model)



            model = PrintModel(
                3,
                "· · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · ·",
                1,
                wordSize,
                0,
                bold = 1
            )
            list.add(model)

            /*  ${if (type == 0) "一类区域" else if (type == 1) "二类区域" else ""}   */
            model = PrintModel(1, chRule, 1, wordSize, 0)
            list.add(model)

            model = PrintModel(
                3,
                "· · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · ·",
                1,
                wordSize,
                0,
                bold = 1
            )
            list.add(model)

            model = PrintModel(
                1,
                "温馨提示：请关好车窗，锁好车门，防止车内物品被盗；关注${weichat_name}公众号，享受更多服务功能.",
                1,
                wordSize,
                0,
                bold = 1
            )
            list.add(model)

            model = PrintModel(
                3,
                "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",
                1,
                wordSize,
                0,
                bold = 1
            )
            list.add(model)

            model = PrintModel(
                1, mianze,
                1, wordSize, 0, bold = 1
            )
            list.add(model)

            model = PrintModel(
                3,
                "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",
                1,
                wordSize,
                0,
                bold = 1
            )
            list.add(model)

            model = PrintModel(
                1, "价格监督:$jindunumber",
                1, wordSize, 0
            )
            list.add(model)

            model = PrintModel(
                1,
                "服务热线:$tousuphone(工作时段)",
                1,
                wordSize,
                10
            )
            model.isChangeLine = false
            list.add(model)


        }
        PrintData(list)
    }


    /**
     * @description : 彭山出车小票
     * @creation date: 2020/9/21  9:36
     * @User user:Administrator
     * @param :
     * @Return :
     */
    private fun Printout_S(checkInOutResponseModel: CheckInOutResponseModel, REFOUD_OUT: Boolean) {
        val wordSize = 24
        val list = ArrayList<PrintModel>()
        checkInOutResponseModel.apply {
            var model = PrintModel(1, "***********停车票据***********", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "操作    :$state", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "停车站点:$parkName", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "车位编号:$lotCode", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "进场时间:$inTime", 1, wordSize, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "出场时间:$outTime", 1, wordSize, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "应收金额:$fee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "预交费金额:$preFee", 1, wordSize, 10)
            list.add(model)

            model = PrintModel(1, "本次追缴金额:$amount", 1, wordSize, 10)
            list.add(model)

            if (REFOUD_OUT) {
                model = PrintModel(1, "退款出车   退款金额 :$preFee", 1, wordSize, 10)
                list.add(model)
            } else {
                model = PrintModel(1, "实收金额:${realFee + amount}", 1, wordSize, 10)
                list.add(model)
            }



            model = PrintModel(1, "停车时长:${DateUtils.converday(outTime, inTime)}", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "车牌号码:$plateNumber", 1, wordSize, 10, bold = 1)
            list.add(model)
            when (carType) {
                CarRecording.UserType.TEMP -> {
                    model = PrintModel(1, "停车类型:临停", 1, wordSize, 10)
                    list.add(model)
                }

                CarRecording.UserType.WECHAT_COMPANY_USER -> {
                    model = PrintModel(1, "停车类型:微信企业", 1, wordSize, 10)
                    list.add(model)
                }
                CarRecording.UserType.CHARE_USER -> {
                    model = PrintModel(1, "停车类型:微信充值", 1, wordSize, 10)
                    list.add(model)
                }
                CarRecording.UserType.WECHAT_PAY_USER -> {
                    model = PrintModel(1, "停车类型:微信用户缴费", 1, wordSize, 10)
                    list.add(model)
                }
                CarRecording.UserType.ELECTRONIC_USER -> {
                    model = PrintModel(1, "停车类型:新能源车", 1, wordSize, 10)
                    list.add(model)
                }
            }

            model = PrintModel(1, "收费员联系电话:$inManPhoneNo", 1, wordSize, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(
                3,
                "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -----------------------------------",
                1,
                24,
                0,
                bold = 1
            )
            list.add(model)
            model = PrintModel(
                1, "温馨提示:",
                1, wordSize, 10
            )
            list.add(model)
            model = PrintModel(
                1, "车主您好！您所缴纳的费用为城市道路资源占用费,我公司对停放车辆及财务损失不承担责任。",
                1, wordSize, 10
            )
            list.add(model)
            model = PrintModel(
                3,
                "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -----------------------------------",
                1,
                wordSize,
                0,
                bold = 1
            )
            list.add(model)




            try {
                if (gongzonghao != null && "" != gongzonghao) {
                    model = PrintModel(
                        2, gongzonghao!!,
                        2, 30, 40
                    )
                    model.isChangeLine = false
                    list.add(model)
                    model = PrintModel(
                        1, "扫码查询缴纳欠费",
                        2, 28, 20, bold = 1
                    )
                    list.add(model)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            model = PrintModel(
                1,
                "推荐关注" + weichat_name + "微信公众号,提供车位余位查询、欠费补缴、电子发票申领等在线服务功能。",
                1,
                wordSize,
                0
            )
            list.add(model)

        }


        PrintData(list)
    }


    /**
     * @description : 补缴欠费打印小票
     *  data
     *  @param type 1 单一打印 type 2 ALL
     *  @param totalFee  var totalFee: Float// 欠费总金额
     * @creation date: 2020/7/10  10:29
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun printConst_S(
        data: CheckCostRecord? = null,
        type: Int,
        data2: CarRecording? = null,
        totalFee: Float
    ) {
        val wordSize = 24
        var printdata = data
        if (data == null && data2 != null) {
            data2.apply {
                printdata = CheckCostRecord(
                    plateNumber = plateNumber,
                    parkName = parkName,
                    lotCode = spce_code,
                    inManName = inManName,
                    inTime = intime,
                    outTime = outtime!!,
                    fee = fee!!,
                    recFee = recFee!!,
                    arrFee = recFee!!,
                    outManName = outManName!!,
                    parkingTime = parkingTime!!,
                    pid = id.toString(),
                    time = DateUtils.date2Str(Date()),
                    constUsername = ""
                )
            }

        }

        val list = ArrayList<PrintModel>()


        printdata?.apply {
            var model = PrintModel(1, "**********补缴票据**********", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)

            model = PrintModel(1, "车牌号码:$plateNumber", 1, 24, 10, bold = 1)
            list.add(model)
            if (type == 1) {
                model = PrintModel(1, "停车地点:$parkName", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "车位编号:$lotCode", 1, 24, 10)
                list.add(model)
                model = PrintModel(1, "收费员姓名:$constUsername", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "停车人员姓名:$inManName", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "欠费补缴人员:$constUsername", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "进场时间:$inTime", 1, wordSize, 10)
                model.isChangeLine = false
                list.add(model)
                model = PrintModel(1, "离场时间:$outTime", 1, wordSize, 10)
                model.isChangeLine = false
                list.add(model)
                val dateOut = DateUtils.str2Date(outTime, DateUtils.FORMAT)
                //  val dateIn = DateUtils.str2Date(inTime, DateUtils.FORMAT)
                if (dateOut != null) {
                    model = PrintModel(
                        1,
                        "停车时长:" + DateUtils.converday(outTime, inTime),
                        1,
                        wordSize,
                        10
                    )
                    list.add(model)
                }
                model = PrintModel(1, "应缴费用:$fee 实缴费用:$recFee ", 1, wordSize, 10)
                list.add(model)
            } else {
                model = PrintModel(1, "停车地点:所有站点 所有车位", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "收费员姓名:所有人员", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "停车人员姓名:所有人员", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "进场时间:所有时段", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "离场时间:所有时间", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "停车时长:所有时间", 1, wordSize, 10)
                list.add(model)
            }

            model = PrintModel(
                1,
                "欠费总金额:${if (totalFee == 0f) "***" else totalFee.toString()}",
                1,
                wordSize,
                10
            )
            list.add(model)
            model = PrintModel(1, "本次补缴金额:$arrFee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(
                1,
                "补缴时间:${if (time == "") DateUtils.getCurDateStr() else time}",
                1,
                wordSize,
                10
            )
            model.isChangeLine = false
            list.add(model)


        }


        PrintData(list)
    }


    /**
     * @description : 补打欠费追缴票据
     * @creation date: 2020/7/10  10:28
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun printConstBill_S(data: CheckCostRecord) {
        val list = ArrayList<PrintModel>()
        val wordSize = 24
        data.apply {
            var model = PrintModel(1, "**********补缴票据**********", 1, 20, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "车牌号码:$plateNumber", 1, 24, 10, bold = 1)
            list.add(model)
            model = PrintModel(1, "停车地点:$parkName", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "收费员姓名:$inManName", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "停车人员姓名:$inManName", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "补缴人员姓名:${constUsername}", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "进场时间:$inTime", 1, wordSize, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "离场时间:$outTime", 1, wordSize, 10)
            model.isChangeLine = false
            list.add(model)
            val dateOut = DateUtils.str2Date(outTime, DateUtils.FORMAT)
            // val dateIn = DateUtils.str2Date(inTime, DateUtils.FORMAT)
            if (dateOut != null) {
                //  val time = (dateOut.time - dateIn.time) / 1000 / 60
                model =
                    PrintModel(1, "停车时长:" + DateUtils.converday(outTime, inTime), 1, wordSize, 10)
                list.add(model)
            }
            model = PrintModel(1, "应缴费用:$fee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "实缴费用:$recFee", 1, wordSize, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "欠费总金额:$arrFee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "本次补缴金额:$fee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "补缴时间:$time", 1, wordSize, 10)
            model.isChangeLine = false
            list.add(model)
        }

        PrintData(list)
    }


    /**
     * @name  注解  今日报表 彭山
     * @class name:PrintMeanger.kt
     * @anthor user: Administrator
     * @time 2020/7/9 12:08
     * @change
     * @chang 12:08
     * @version 1.0
     */
    fun printInfoIn_S(data: DayReportRes) {
        val list = ArrayList<PrintModel>()
        val wordSize = 24
        data.apply {
            var model = PrintModel(1, "**********今日报表**********", 1, 24, 20)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(
                1,
                "姓名:${username}",
                1,
                20,
                10
            )
            list.add(model)
            model = PrintModel(1, "日期:$time", 1, wordSize, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "打印时间:${DateUtils.getCurDateStr()}", 1, wordSize, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "站点:$parkName", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "现金收费总额:$moneyFee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "现金临停收费:$moneyRecFee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "现金欠费追缴:$moneyArrFee", 1, wordSize, 10)
            list.add(model)

            model = PrintModel(1, "非现金收费总额:$notMoneyFee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "非现金临停收费:$notMoneyRecFee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "非现金欠费追缴:$notMoneyArrFee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(1, "今日收费总额:$sumFee", 1, 24, 10)
            list.add(model)
        }

        PrintData(list)
    }


    /** <--------------------------------------------------彭 州 原 始 方 案----------------------------------------------------->**/


    /**
     * @description : 彭州进车小票
     * @creation date: 2020/9/21  9:36
     * @User user:Administrator
     * @param :
     * @Return :
     */
    private fun Printin_Z(checkInOutResponseModel: CheckInOutResponseModel) {
        val list = ArrayList<PrintModel>()
        checkInOutResponseModel.apply {
            var model = PrintModel(1, "**********停车票据**********", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "操作    :${state}", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "进场时间:$inTime", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "停车站点:${siteName}", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "车位编号:$lotCode", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "车牌号码:$plateNumber", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "收费员  :$inManName", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "电话    :$inManPhoneNo", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            if (isArr) {
                model = PrintModel(1, "欠费笔数:${arrCount}笔  金额:${sumArrFee}元", 1, 24, 0)
                model.isChangeLine = false
                list.add(model)
            }

            model = PrintModel(1, "收费标准:$chRule", 1, 24, 10)
            list.add(model)
            model = PrintModel(
                1, mianze,
                1, 24, 10
            )
            list.add(model)
            model = PrintModel(
                1, tousuphone,
                1, 24, 10
            )
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(
                1, "管理单位:",
                1, 24, 10
            )
            list.add(model)
            model = PrintModel(
                1, company_name,
                1, 24, 10
            )
            list.add(model)

            try {
                if (arrreocept != null && "" != arrreocept) {

                    model = PrintModel(
                        2, arrreocept + "$id",
                        2, 10, 40
                    )
                    list.add(model)
                    var boottomds = 40
                    if ("" != chRule) {
                        boottomds = 80
                    }
                    model = PrintModel(
                        1, "扫码支付或呼叫收费员",
                        2, 24, boottomds
                    )
                    list.add(model)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


        }
        Log.i("printlibr", "配置彭州打印方案")
        PrintData(list)
    }

    /**
     * @description : 彭州出车小票
     * @creation date: 2020/9/21  9:36
     * @User user:Administrator
     * @param :
     * @Return :
     */
    private fun Printout_Z(
        checkInOutResponseModel: CheckInOutResponseModel,
        REFOUD_OUT: Boolean = false
    ) {
        val list = ArrayList<PrintModel>()
        checkInOutResponseModel.apply {

            var model = PrintModel(1, "**********停车票据**********", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "操作    :出场", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "停车站点:$parkName", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "车位编号:$lotCode", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "进场时间:$inTime", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "出场时间:$outTime", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "应收金额:$fee", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "预交费金额:$preFee", 1, 24, 10)
            list.add(model)

            model = PrintModel(1, "本次追缴金额:$amount", 1, 24, 10)
            list.add(model)

            if (REFOUD_OUT) {
                model = PrintModel(1, "退款出车   退款金额:$preFee", 1, 24, 10)
                list.add(model)
            } else {
                model = PrintModel(1, "实收金额:${realFee + amount}", 1, 24, 10)
                list.add(model)
            }


            model = PrintModel(1, "停车时长:${DateUtils.converday(outTime, inTime)}", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "车牌号码:$plateNumber", 1, 24, 10)
            list.add(model)
            when (carType) {
                CarRecording.UserType.TEMP -> {
                    model = PrintModel(1, "停车类型:临停", 1, 24, 10)
                    list.add(model)
                }

                CarRecording.UserType.WECHAT_COMPANY_USER -> {
                    model = PrintModel(1, "停车类型:微信企业", 1, 24, 10)
                    list.add(model)
                }
                CarRecording.UserType.CHARE_USER -> {
                    model = PrintModel(1, "停车类型:微信充值", 1, 24, 10)
                    list.add(model)
                }
                CarRecording.UserType.WECHAT_PAY_USER -> {
                    model = PrintModel(1, "停车类型:微信用户缴费", 1, 24, 10)
                    list.add(model)
                }
                CarRecording.UserType.ELECTRONIC_USER -> {
                    model = PrintModel(1, "停车类型:新能源车", 1, 24, 10)
                    list.add(model)
                }
            }


            model = PrintModel(1, "收费员  :$inManName", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "电话    :$inManPhoneNo", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(
                1, "温馨提示:",
                1, 24, 10
            )
            list.add(model)
            model = PrintModel(
                1, "车主您好！您所缴纳的费用为城市道路资源占用费,我公司对停放车辆及财务损失不承担责任。",
                1, 24, 10
            )
            list.add(model)

            try {
                if (gongzonghao != null && "" != gongzonghao) {
                    model = PrintModel(
                        2, gongzonghao!!,
                        2, 10, 40
                    )
                    model.isChangeLine = false
                    list.add(model)
                    model = PrintModel(
                        1, "扫码查询缴纳欠费",
                        2, 24, 20
                    )
                    list.add(model)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            PrintData(list)
        }
    }

    /**
     * @description : 补缴欠费打印小票
     *  data
     *  @param type 1 单一打印 type 2 ALL
     *  @param totalFee  var totalFee: Float// 欠费总金额
     * @creation date: 2020/7/10  10:29
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun printConst_Z(
        data: CheckCostRecord? = null,
        type: Int,
        data2: CarRecording? = null,
        totalFee: Float
    ) {
        val wordSize = 24
        var printdata = data
        if (data == null && data2 != null) {
            data2.apply {
                printdata = CheckCostRecord(
                    plateNumber = plateNumber,
                    parkName = parkName,
                    lotCode = spce_code,
                    inManName = inManName,
                    inTime = intime,
                    outTime = outtime!!,
                    fee = fee!!,
                    recFee = recFee!!,
                    arrFee = recFee!!,
                    outManName = outManName!!,
                    parkingTime = parkingTime!!,
                    pid = id.toString(),
                    time = DateUtils.date2Str(Date()),
                    constUsername = ""
                )
            }

        }


        val list = ArrayList<PrintModel>()


        printdata?.apply {
            var model = PrintModel(1, "**********补缴票据**********", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)

            model = PrintModel(1, "车牌号码:$plateNumber", 1, 24, 10, bold = 1)
            list.add(model)
            if (type == 1) {
                model = PrintModel(1, "停车地点:$parkName", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "车位编号:$lotCode", 1, 24, 10)
                list.add(model)
                model = PrintModel(1, "收费员姓名:$constUsername", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "停车人员姓名:$inManName", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "欠费补缴人员:$constUsername", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "进场时间:$inTime", 1, wordSize, 10)
                model.isChangeLine = false
                list.add(model)
                model = PrintModel(1, "离场时间:$outTime", 1, wordSize, 10)
                model.isChangeLine = false
                list.add(model)
                val dateOut = DateUtils.str2Date(outTime, DateUtils.FORMAT)
                //  val dateIn = DateUtils.str2Date(inTime, DateUtils.FORMAT)
                if (dateOut != null) {
                    model = PrintModel(
                        1,
                        "停车时长:" + DateUtils.converday(outTime, inTime),
                        1,
                        wordSize,
                        10
                    )
                    list.add(model)
                }
                model = PrintModel(1, "应缴费用:$fee 实缴费用:$recFee ", 1, wordSize, 10)
                list.add(model)
            } else {
                model = PrintModel(1, "停车地点:所有站点 所有车位", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "收费员姓名:所有人员", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "停车人员姓名:所有人员", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "进场时间:所有时段", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "离场时间:所有时间", 1, wordSize, 10)
                list.add(model)
                model = PrintModel(1, "停车时长:所有时间", 1, wordSize, 10)
                list.add(model)
            }

            model = PrintModel(
                1,
                "欠费总金额:${if (totalFee == 0f) "***" else totalFee.toString()}",
                1,
                wordSize,
                10
            )
            list.add(model)
            model = PrintModel(1, "本次补缴金额:$arrFee", 1, wordSize, 10)
            list.add(model)
            model = PrintModel(
                1,
                "补缴时间:${if (time == "") DateUtils.getCurDateStr() else time}",
                1,
                wordSize,
                10
            )
            model.isChangeLine = false
            list.add(model)


        }


        PrintData(list)
    }


    /**
     * @description : 补打欠费追缴票据
     * @creation date: 2020/7/10  10:28
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun printConstBill_Z(data: CheckCostRecord) {
        val list = ArrayList<PrintModel>()

        data.apply {
            var model = PrintModel(1, "**********补缴票据**********", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "车牌号码:$plateNumber", 1, 24, 10, bold = 1)
            list.add(model)
            model = PrintModel(1, "停车地点:$parkName", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "收费员姓名:$inManName", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "停车人员姓名:$inManName", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "补缴人员姓名:${constUsername}", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "进场时间:$inTime", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "离场时间:$outTime", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            val dateOut = DateUtils.str2Date(outTime, DateUtils.FORMAT)
            // val dateIn = DateUtils.str2Date(inTime, DateUtils.FORMAT)
            if (dateOut != null) {
                //  val time = (dateOut.time - dateIn.time) / 1000 / 60
                model = PrintModel(1, "停车时长:" + DateUtils.converday(outTime, inTime), 1, 24, 10)
                list.add(model)
            }
            model = PrintModel(1, "应缴费用:$fee", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "实缴费用:$recFee", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "欠费总金额:$arrFee", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "本次补缴金额:$fee", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "补缴时间:$time", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
        }

        PrintData(list)
    }

    /**
     * @name  注解  今日报表 彭州
     * @class name:PrintMeanger.kt
     * @anthor user: Administrator
     * @time 2020/7/9 12:08
     * @change
     * @chang 12:08
     * @version 1.0
     */
    fun printInfoIn_Z(data: DayReportRes) {
        val list = ArrayList<PrintModel>()

        data.apply {
            var model = PrintModel(1, "**********今日报表**********", 1, 24, 20)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "姓名:${username}", 1, 20, 10)
            list.add(model)
            model = PrintModel(1, "日期:$time", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "打印时间:${DateUtils.getCurDateStr()}", 1, 24, 10)
            model.isChangeLine = false
            list.add(model)
            model = PrintModel(1, "站点:$parkName", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "现金收费总额:$moneyFee", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "现金临停收费:$moneyRecFee", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "现金欠费追缴:$moneyArrFee", 1, 24, 10)
            list.add(model)

            model = PrintModel(1, "非现金收费总额:$notMoneyFee", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "非现金临停收费:$notMoneyRecFee", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "非现金欠费追缴:$notMoneyArrFee", 1, 24, 10)
            list.add(model)
            model = PrintModel(1, "今日收费总额:$sumFee", 1, 24, 10)
            list.add(model)
        }

        PrintData(list)
    }
}

interface connectListener {
    fun connect(state: ConnectState, error: String?)

    fun doOnComplete() {

    }

    fun doOnError(error: String) {

    }

    fun doStart() {

    }

    fun doFailrue() {

    }
}

@Parcelize
data class Bluedevice(var address: String, var name: String) : Parcelable
