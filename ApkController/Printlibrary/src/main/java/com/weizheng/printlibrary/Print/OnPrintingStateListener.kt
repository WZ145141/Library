package com.weizheng.printlibrary.Print

interface OnPrintingStateListener {
    fun doOnComplete()
    fun doOnError(error: String)
    fun doStart()
    fun doFailrue()

}