package com.weizheng.printlibrary.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.weizheng.printlibrary.R


/**
 * 自定义RecycleView分割线
 */
class RecycleViewDivider(context : Context, orientation: Int) : RecyclerView.ItemDecoration(){

    private var paint: Paint? = Paint(Paint.ANTI_ALIAS_FLAG)
    private var divider: Drawable? = null
    private var dividerHeight = 1//分割线高度，默认为1px
    //列表的方向：LinearLayoutManager.VERTICAL或LinearLayoutManager.HORIZONTAL
    private var orientation: Int = LinearLayoutManager.HORIZONTAL
    private val attrs = intArrayOf(android.R.attr.listDivider)
    private var isDrawableLastItem: Boolean = true

    init {
        this.orientation = orientation
        paint?.color = ContextCompat.getColor(context, R.color.color_dddddd)
        paint?.style = Paint.Style.FILL
        val a = context.obtainStyledAttributes(attrs)
        divider = a.getDrawable(0)
        a.recycle()
    }

    //绘制分割线
    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)
        if (orientation == LinearLayoutManager.VERTICAL) {
            drawVertical(c, parent)
        } else {
            drawHorizontal(c, parent)
        }
    }

    //绘制分割线
    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)
        if (orientation == LinearLayoutManager.VERTICAL) {
            drawVertical(c, parent)
        } else {
            drawHorizontal(c, parent)
        }
    }

    //绘制横向 item 分割线
    private fun drawHorizontal(canvas: Canvas, parent: RecyclerView) {
        val left = parent.paddingLeft
        val right = parent.measuredWidth - parent.paddingRight
        val childSize = parent.childCount
        for (i in 0 until childSize) {
            val child = parent.getChildAt(i)
            val layoutParams = child.layoutParams as RecyclerView.LayoutParams
            if (!isDrawableLastItem && i == childSize - 1) { //判断是否绘制最后一个item的分割线，解决最后一个分割线导致高度不一致的问题
                layoutParams.bottomMargin = 0
                child.layoutParams = layoutParams
                break
            }
            layoutParams.bottomMargin = dividerHeight
            child.layoutParams = layoutParams
            // 意思就是说，要求下边伸出一部分来，用于绘制这个分割线，layoutParams.bottomMargin与View的实际高度无关
            val top = child.bottom
            val bottom = top + dividerHeight
            if (divider != null) {
                divider!!.setBounds(left, top, right, bottom)
                divider!!.draw(canvas)
            }
            if (paint != null) {
                canvas.drawRect(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat(), paint!!)
            }
        }
    }

    //绘制纵向 item 分割线
    private fun drawVertical(canvas: Canvas, parent: RecyclerView) {
        val top = parent.paddingTop
        val bottom = parent.measuredHeight - parent.paddingBottom
        val childSize = parent.childCount
        for (i in 0 until childSize) {
            val child = parent.getChildAt(i)
            val layoutParams = child.layoutParams as RecyclerView.LayoutParams
            if (!isDrawableLastItem && i == childSize - 1) { //判断是否绘制最后一个item的分割线，解决最后一个分割线导致宽度不一致的问题
                layoutParams.rightMargin = 0
                child.layoutParams = layoutParams
                break
            }
            layoutParams.rightMargin = dividerHeight
            child.layoutParams = layoutParams
            // 意思就是说，要求右边伸出一部分来，用于绘制这个分割线，layoutParams.bottomMargin与View的实际宽度无关
            val left = child.right
            val right = left + dividerHeight
            if (divider != null) {
                divider!!.setBounds(left, top, right, bottom)
                divider!!.draw(canvas)
            }
            if (paint != null) {
                canvas.drawRect(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat(), paint!!)
            }
        }
    }

    /**
     * 设置是否绘制最后一个chaildView的分割线.
     * @param bool
     */
    fun setDrawLastItem(bool: Boolean) {
        isDrawableLastItem = bool
    }


}