package com.weizheng.printlibrary.Print


//import zpCPCLSDK.zpCPCLSDK.zp_cpcl_BluetoothPrinter
import android.content.Context
import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.math.floor

/**
 * 打印机操作
 */

class PrintUtil2 : BasePrintAdapter {

    private object Helper {
        val instance = PrintUtil2()
    }

    companion object {
        fun getInstance() = Helper.instance

        private var isConnected: Boolean = false
    }

    var printerInterface: zp_cpcl_BluetoothPrinter? = null
    override fun initprinter(context: Context) {
        printerInterface = zp_cpcl_BluetoothPrinter(context)

    }

    class ConnectStatus(val isSuccess: Boolean, val message: String, val code: Int)

    var connectLister: OnPrintconnetListener? = null
    var connectstateListener: OnPrintingStateListener? = null

    /**
     * @description ： 是否已连接
     * @creation date: 2020/7/10  10:59
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun isConnected(): Boolean {
        return isConnected
    }


    /**
     * @description ： 断开连接
     * @creation date: 2020/7/10  10:59
     * @User user:Administrator
     * @param :
     * @Return :
     */
    fun disconnect() {
        try {

            printerInterface?.disconnect()
            connectLister?.OnConnetState(ConnectState.FAILURE, "断开连接！")
            Log.i("printlibr", "断开连接")
            isConnected = false
            connectLister = null
            connectstateListener = null
        } catch (e: Exception) {
        }
    }

    /**
     * 连接打印机,默认连接失败
     */
    private fun connect(address: String): Observable<ConnectStatus> {
        var status: ConnectStatus

        if (!isConnected()) {

            return Observable.create<ConnectStatus> {
                printerInterface?.let { Interface ->
                    if (!Interface.connect(address)) {
                        status = ConnectStatus(true, "连接失败", -1)
                        // disconnect()
                        it.onNext(status)

                        return@create
                    }
                    Interface.printerStatus()
                    when (Interface.GetStatus()) {
                        0 -> {
                            status = ConnectStatus(true, "连接成功", 0)
                            isConnected = true
                        }
                        1 -> {
                            status = ConnectStatus(false, "打印机缺纸", 1)
                        }
                        2 -> {
                            status = ConnectStatus(false, "打印机纸仓盖开", 2)
                        }
                        3 -> {
                            status = ConnectStatus(false, "打印头过热", 3)
                        }
                        -1 -> {
                            status = ConnectStatus(false, "打印机没反应", -1)
                            // disconnect()

                        }
                        else -> {
                            status = ConnectStatus(false, "未知错误", 5)
                            disconnect()
                        }
                    }
                    it.onNext(status)
                }
                if (printerInterface == null) {
                    Log.i("printlibr", "printerInterface 未初始化")
                    status = ConnectStatus(false, "printerInterface 未初始化", 0)
                    it.onNext(status)

                }

            }
        } else {


            return Observable.create<ConnectStatus> {
                if (printerInterface == null) {
                    Log.i("printlibr", "printerInterface 未初始化")
                    status = ConnectStatus(false, "printerInterface 未初始化", 0)
                    it.onNext(status)

                } else {
                    printerInterface?.printerStatus()
                    when (printerInterface?.GetStatus()) {
                        0 -> {

                            status = ConnectStatus(true, "连接成功", 0)
                            isConnected = true
                        }
                        1 -> {
                            status = ConnectStatus(false, "打印机缺纸", 1)
                        }
                        2 -> {
                            status = ConnectStatus(false, "打印机纸仓盖开", 2)
                        }
                        3 -> {
                            status = ConnectStatus(false, "打印头过热", 3)
                        }
                        -1 -> {

                            status = ConnectStatus(false, "打印机没反应", -1)
                            // disconnect()
                            printerInterface?.disconnect()
                            isConnected = false
                            try {
                                if (!printerInterface?.connect(address)!!) {
                                    status = ConnectStatus(true, "连接失败", 6)
                                    // disconnect()
                                    it.onNext(status)
                                    return@create
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            printerInterface?.printerStatus()
                            when (printerInterface?.GetStatus()) {
                                0 -> {
                                    status = ConnectStatus(true, "连接成功", 0)
                                    isConnected = true

                                }
                                else -> {
                                    status = ConnectStatus(false, "未知错误", 5)

                                }
                            }
                        }
                        else -> {
                            status = ConnectStatus(false, "未知错误", 5)
                            disconnect()
                        }
                    }

                    it.onNext(status)
                }

            }
        }
    }

    /**
     * 打印信息，入参请参照PringModel进行传值
     * 打印之前请先连接，model值请按打印顺序添加
     */
    private fun printData(printList: ArrayList<PrintModel>): Int {
        if (printerInterface == null) {
            Log.i("printlibr", "printerInterface 未初始化")
            return 0
        }
        Log.i("printlibr", "print打印中")
        var totalHeight = 0
        val totalWidth = 426//576
        //计算高度
        loop@ for (model in printList) {

            when (model.wordSize) {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 -> {

                    model.wordSize = 18
                }
                20, 21, 22, 23 -> {

                    model.wordSize = 20

                }
                24, 25, 26 -> {
                    model.wordSize = 23

                }
                27, 28, 29 -> {
                    model.wordSize = 28
                }
                30, 31, 32 -> {

                    model.wordSize = 32

                }
                else -> {
                    model.wordSize = 48

                }
            }

            when (model.type) {
                1 -> {
                    val contentLen = model.content.length
                    if (contentLen == 0) {
                        continue@loop
                    }
                    var lines = 1
                    if (model.isChangeLine) {


                        lines = if (contentLen % PrintModel.WORD_NUM_LINE == 0) {
                            contentLen / PrintModel.WORD_NUM_LINE
                        } else {
                            contentLen / PrintModel.WORD_NUM_LINE + 1
                        }
                    }

                    /*  for (pos in 1..lines) {
                          totalHeight += (model.wordSize!! + PrintModel.LINE_GAP)
                          totalHeight += model.bottomDistance!!
                      }*/
                    totalHeight += (model.wordSize!! + PrintModel.LINE_GAP) * lines
                    totalHeight += model.bottomDistance!!


                }
                2 -> {
                    totalHeight += PrintModel.IMG_HEIGHT
                    totalHeight += PrintModel.LINE_GAP
                    totalHeight += model.bottomDistance!!
                }
                3 -> {
                    totalHeight += 14 + PrintModel.LINE_GAP
                    totalHeight += model.bottomDistance!!
                }
            }


        }
        totalHeight += 120

        //开始打印
        printerInterface?.apply {

            this.pageSetup(totalWidth, totalHeight)
            var currentHeight = 20
            var currentWidth = 0
            loop@ for (model in printList) {

                var type: Int
                when (model.wordSize) {
                    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 -> {
                        type = 1
                        PrintModel.WORD_NUM_LINE = 21
                        model.wordSize = 18
                    }
                    20, 21, 22 -> {
                        type = 2
                        PrintModel.WORD_NUM_LINE = 19
                        model.wordSize = 20
                    }
                    23, 24, 25, 26 -> {
                        type = 3
                        model.wordSize = 23
                        PrintModel.WORD_NUM_LINE = 16
                    }
                    27, 28, 29 -> {
                        type = 0
                        PrintModel.WORD_NUM_LINE = 14
                        model.wordSize = 28
                    }

                    30, 31, 32 -> {
                        type = 4
                        model.wordSize = 32
                        PrintModel.WORD_NUM_LINE = 12
                    }
                    else -> {
                        model.wordSize = 48
                        type = 5
                        PrintModel.WORD_NUM_LINE = 8
                    }
                }
                val a = (totalWidth / (model.wordSize!! + PrintModel.LINE_GAP)).toDouble()
                PrintModel.WORD_NUM_LINE = floor(a).toInt()
                when (model.type) {
                    1 -> {
                        val contentLen = model.content.length
                        if (contentLen == 0) {
                            continue@loop
                        }
                        var lines: Int
                        var lastLineNum: Int = PrintModel.WORD_NUM_LINE

                        var startIndex = 0
                        var endIndex = 0

                        if (contentLen % PrintModel.WORD_NUM_LINE == 0) {
                            lines = contentLen / PrintModel.WORD_NUM_LINE
                            lastLineNum = PrintModel.WORD_NUM_LINE
                        } else {
                            lines = contentLen / PrintModel.WORD_NUM_LINE + 1
                            lastLineNum = contentLen % PrintModel.WORD_NUM_LINE
                        }



                        if (!model.isChangeLine) {
                            lines = 1
                        }
                        for (pos in 1..lines) {
                            endIndex += if (pos == lines) {
                                lastLineNum
                            } else {
                                PrintModel.WORD_NUM_LINE
                            }
                            val content: String = if (model.isChangeLine) {
                                model.content.substring(startIndex, endIndex)
                            } else {
                                model.content
                            }
                            when (model.location) {
                                1 ->
                                    currentWidth = PrintModel.LINE_GAP
                                2 -> {
                                    currentWidth =
                                        (totalWidth - model.wordSize!! * model.content.length + PrintModel.LINE_GAP) / 2
                                }

                                3 ->
                                    currentWidth = totalWidth - model.wordSize!!
                            }
                            drawText(
                                currentWidth,
                                currentHeight,
                                content,
                                type,
                                0,
                                model.bold!!,
                                false,
                                false
                            )
                            currentHeight += model.bottomDistance!!
                            startIndex += content.length
                            currentHeight += model.wordSize!!
                            currentHeight += PrintModel.LINE_GAP
                            currentWidth = 0
                        }
                    }
                    2 -> {
                        when (model.location) {
                            1 ->
                                currentWidth = PrintModel.LINE_GAP / 2
                            2 ->
                                currentWidth =
                                        // (totalWidth - PrintModel.IMG_HEIGHT + PrintModel.LINE_GAP) / 2
                                    totalWidth / 2 - PrintModel.IMG_HEIGHT / 2 - PrintModel.LINE_GAP
                            3
                            ->
                                currentWidth =
                                    totalWidth - PrintModel.IMG_HEIGHT - PrintModel.LINE_GAP
                        }
                        //   currentHeight += 10
                        drawQrCode(currentWidth, currentHeight, model.content, 4, 5, 0)
                        currentHeight += PrintModel.IMG_HEIGHT
                        currentHeight += PrintModel.LINE_GAP


                        drawText(
                            currentWidth,
                            currentHeight + model.bottomDistance!!,
                            "",
                            type,
                            0,
                            model.bold!!,
                            false,
                            false
                        )
                        currentHeight += model.bottomDistance!!
                        currentWidth = 0
                    }

                    3 -> {
                        val contentLen = model.content.length
                        if (contentLen == 0) {
                            continue@loop
                        }
                        drawText(
                            currentWidth,
                            currentHeight,
                            model.content,
                            1,
                            0,
                            model.bold!!,
                            false,
                            false
                        )
                        currentHeight += model.bottomDistance!!

                        currentHeight += model.wordSize!!
                        currentHeight += PrintModel.LINE_GAP
                        currentWidth = 0

                    }
                }

            }

            print(0, 0)
            //  getInstance().disconnect()
            //   disconnect()
            return 0
        }
        return 0
    }

    /**
     * @description ： 打印返回
     * @creation date: 2020/7/10  11:00
     * @User user:Administrator
     * @param :
     * @Return :
     */
    private fun printDataForResult(printList: ArrayList<PrintModel>): Observable<Int> {
        return Observable.create<Int> {
            val result = printData(printList)
            it.onNext(result)
            it.onComplete()
        }
    }

    private val compositeDisposable = CompositeDisposable()


    /**
     * @description ： 连接蓝牙
     * @creation date: 2020/7/10  11:01
     * @User user:Administrator
     * @param :
     * @Return :
     */
    override fun connet(address: String): Boolean {
        compositeDisposable.add(connect(address)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                connectLister?.OnConnetState(ConnectState.START)
            }
            .doOnComplete {
            }
            .doOnError {
                // connectLister?.OnConnetState(ConnectState.UNKNOWN, it.message)
            }
            .subscribe {
                when (it.code) {
                    0 -> {
                        connectLister?.OnConnetState(ConnectState.SUCCESS)
                    }
                    1 -> {
                        connectLister?.OnConnetState(ConnectState.NO_PAGER)
                    }
                    2 -> {
                        connectLister?.OnConnetState(ConnectState.UNKNOWN, it.message)
                    }
                    3 -> {
                        connectLister?.OnConnetState(ConnectState.DRIVCE_HOT, it.message)
                    }
                    -1, 6 -> {
                        connectLister?.OnConnetState(ConnectState.NO_RESPONSE)

                    }

                    else -> {
                        connectLister?.OnConnetState(ConnectState.UNKNOWN, it.message)
                    }

                }
            }
        )
        return false
    }

    override fun disconnet(): Boolean {
        disconnect()

        return false
    }

    override fun PrintData(data: ArrayList<PrintModel>) {
        compositeDisposable
            .add(printDataForResult(data)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    connectstateListener?.doStart()
                }
                .doOnComplete {
                    connectstateListener?.doOnComplete()
                }
                .doOnError {
                    connectstateListener?.doFailrue()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({


                }, {

                    connectstateListener?.doOnError(it.message.toString())
                })
            )
    }

    override fun addConnectListener(listener: OnPrintconnetListener) {
        connectLister = listener
    }

    override fun addConnectingListener(listener: OnPrintingStateListener) {
        connectstateListener = listener
    }
}

data class PrintModel(
    //1：文字，2二维码 3:line
    var type: Int? = 1,
    //打印内容，文字展示个数是固定的，如果超过默认会展示多行，图片有默认高度
    var content: String,
    //1：left,2:center,3:right
    var location: Int? = 1,
    //文字大小，建议大小为：14，20，24，28，不可超过28
    var wordSize: Int? = 20,
    var bottomDistance: Int? = 10,
    var bold: Int? = 0
) {
    var isChangeLine: Boolean = true

    companion object {

        const val LINE_GAP = 3

        /* 图片高度*/
        const val IMG_HEIGHT = 194 //156//272

        //一行的字数 21  ,15,13,12
        var WORD_NUM_LINE = 16
    }
}