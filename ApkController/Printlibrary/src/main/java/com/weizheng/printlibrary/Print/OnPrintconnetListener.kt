package com.weizheng.printlibrary.Print

interface OnPrintconnetListener {

    fun OnConnetState(state: ConnectState, error: String? = null)

}

enum class ConnectState() {
    START,
    SUCCESS,
    FAILURE,
    DRIVCE_HOT,
    NO_PAGER,
    NO_RESPONSE,
    UNKNOWN,
    DRICEDNULLPARKING
}