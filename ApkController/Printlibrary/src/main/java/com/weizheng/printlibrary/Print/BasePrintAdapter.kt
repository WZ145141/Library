package com.weizheng.printlibrary.Print

import android.content.Context

interface BasePrintAdapter {
    /**
     * 初始化打印配置
     * @param context 最好传入appcontext
     */
    fun initprinter(context: Context)
    /**
     * 连接
     * @return 是否成功
     */
    fun connet(address: String): Boolean

    /**
     * 断开连接
     * @return 是否成功
     */
    fun disconnet(): Boolean

    /**
     * 打印 格式数据
     */
    fun PrintData(data: ArrayList<PrintModel>)

    /**
     * 连接监听
     */
    fun addConnectListener(listener: OnPrintconnetListener)

    /**
     * 打印监听
     */
    fun addConnectingListener(listener: OnPrintingStateListener)


}