package com.weizheng.printlibrary.bean

import com.google.gson.annotations.SerializedName

/**
 * Created by Simple on 2019-06-04
 * amount 欠费追缴金额
 *
 */
data class CheckInOutResponseModel(
        var state: String,
        var inTime: String,
        var outTime: String,
        var chRule: String,
        var lotCode: String,
        var parkName: String,
        var inManPhoneNo: String,
        var inManNo: String,
        var preFee: Float = 0F,
        var realFee: Float = 0F,
        var totalFee: Float = 0F,
        var outManPhoneNo: String,
        var outManNo: String,
        var isArr: Boolean,
        var inManName: String,
        var fee: Float = 0F,
        var plateNumber: String,
        var siteName: String,
        var carType: Int,
        var id: Long,
        @SerializedName("chRuleType")
        var type: Int = -1,
        var amount: Float = 0F,
        var gongzonghao: String? = "",
        var arrreocept: String? = "",
        var mianze: String,
        var tousuphone: String,
        var company_name: String,
        var weichat_name: String,
        var sumArrFee: Float = 0f,
        var arrCount: Int = 0,
        var cozystr: String,
        var printtime:String,
        var jindunumber:String
)