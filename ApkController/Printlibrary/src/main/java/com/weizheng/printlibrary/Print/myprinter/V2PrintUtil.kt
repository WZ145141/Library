package com.weizheng.printlibrary.Print.myprinter

import android.content.Context
import android.os.RemoteException
import android.util.Log

import com.sunmi.peripheral.printer.InnerResultCallbcak
import com.weizheng.printlibrary.Print.*

/**
 *
 * @Description: 作用描述
 * @Author: V2PrintUtil
 * @CreateDate: 1  16:15
 * @UpdateUser: 更新者
 * @UpdateDate: 1 16:15
 * @UpdateRemark: 更新说明
 * @Version: 1
 */
class V2PrintUtil : BasePrintAdapter {


    private object Helper {
        val instance = V2PrintUtil()
    }

    companion object {
        fun getInstance() = Helper.instance
        private var mContext: Context? = null
    }

    override fun initprinter(context: Context) {
        mContext = context
    }

    override fun connet(address: String): Boolean {
        connectLister?.OnConnetState(ConnectState.START)
        Log.i("printlibr", "v2connet:${SunmiPrintHelper.getInstance().sunmiPrinter}")
        when {
            SunmiPrintHelper.getInstance().sunmiPrinter === SunmiPrintHelper.NoSunmiPrinter -> {
                "no printer"
                connectLister?.OnConnetState(ConnectState.NO_PAGER)
                return false
            }
            SunmiPrintHelper.getInstance().sunmiPrinter === SunmiPrintHelper.CheckSunmiPrinter -> {
                "connecting"
                connectLister?.OnConnetState(ConnectState.NO_RESPONSE)
                return false
            }
            SunmiPrintHelper.getInstance().sunmiPrinter === SunmiPrintHelper.FoundSunmiPrinter -> {
                ""


                connectLister?.OnConnetState(ConnectState.SUCCESS)
                return true
            }
            else -> {
                SunmiPrintHelper.getInstance().initSunmiPrinterService(mContext)
                connectLister?.OnConnetState(ConnectState.SUCCESS)
                return true
            }
        }
    }

    override fun disconnet(): Boolean {
        connectLister = null
        connectstateListener = null
        return true
    }

    override fun PrintData(data: ArrayList<PrintModel>) {


        Log.i("printlibr", "v2connet: start print")
        connectstateListener?.doStart()


        data.let {
            try {


                SunmiPrintHelper.getInstance()
                    .printsumContent(mContext, object : InnerResultCallbcak() {
                        override fun onRunResult(p0: Boolean) {

                        }

                        override fun onReturnString(p0: String?) {

                        }

                        override fun onRaiseException(p0: Int, p1: String?) {

                        }

                        override fun onPrintResult(p0: Int, p1: String?) {
                            if (p0 == 0) connectstateListener?.doOnComplete() else connectstateListener?.doFailrue()
                        }
                    }, data)

            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }


    }


    var connectLister: OnPrintconnetListener? = null
    var connectstateListener: OnPrintingStateListener? = null
    override fun addConnectListener(listener: OnPrintconnetListener) {
        connectLister = listener
    }

    override fun addConnectingListener(listener: OnPrintingStateListener) {
        connectstateListener = listener
    }

}