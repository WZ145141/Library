package com.weizheng.printlibrary.bean

import android.os.Parcel
import android.os.Parcelable


data class CheckCostRecord(
    var arrFee: Float, //欠费金额
    var fee: Float,  //应缴金额
    var inManName: String, //进场人姓名
    var inTime: String,    //进场时间
    var lotCode: String,   //车位编号
    var outManName: String, //出场人姓名
    var outTime: String,  //出场时间
    var parkName: String, //站点名称
    var parkingTime: String, //停车时长
    var pid: String,         //该条停车记录的pid
    var plateNumber: String, //车牌号码
    var recFee: Float,     //实缴金额
    var isCheck: Boolean = false, //选中
    var time: String? = "", // 补缴时间
    var constUsername: String//补缴人员

) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readFloat(),
        parcel.readFloat(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readFloat(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readString().toString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(arrFee)
        parcel.writeFloat(fee)
        parcel.writeString(inManName)
        parcel.writeString(inTime)
        parcel.writeString(lotCode)
        parcel.writeString(outManName)
        parcel.writeString(outTime)
        parcel.writeString(parkName)
        parcel.writeString(parkingTime)
        parcel.writeString(pid)
        parcel.writeString(plateNumber)
        parcel.writeFloat(recFee)
        parcel.writeByte(if (isCheck) 1 else 0)
        parcel.writeString(time)
        parcel.writeString(constUsername)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CheckCostRecord> {
        override fun createFromParcel(parcel: Parcel): CheckCostRecord {
            return CheckCostRecord(parcel)
        }

        override fun newArray(size: Int): Array<CheckCostRecord?> {
            return arrayOfNulls(size)
        }
    }
}