package com.weizheng.printlibrary.bean

//username 收费员姓名
data class DayReportRes(
        var moneyArrFee: String,
        var moneyFee: String,
        var moneyRecFee: String,
        var notMoneyArrFee: String,
        var notMoneyFee: String,
        var notMoneyRecFee: String,
        var parkName: String,
        var realName: String,
        var sumFee: String,
        var time: String,
        var arrFee: String,
        var username:String
)