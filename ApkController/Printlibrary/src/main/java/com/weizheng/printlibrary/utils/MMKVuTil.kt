package com.weizheng.printlibrary.utils


import com.tencent.mmkv.MMKV



/**
 *
 * @Description: MMKV  使用定义区
 * @Author: MMKVuTil
 * @CreateDate: 20201126  12:21
 * @UpdateUser: 更新者
 * @UpdateDate: 20201126 12:21
 * @UpdateRemark: 更新说明
 * @Version: 1
 */
class MMKVuTil {

    val mmkv = MMKV.mmkvWithID("MMKVData", MMKV.MULTI_PROCESS_MODE)
    private val mmkvspace = MMKV.mmkvWithID("MMKVspace", MMKV.MULTI_PROCESS_MODE)


    /**
     *
     * @Description: 添加冲突数
     * @Author: 魏征
     * @CreateDate: 2020/11/30 11:49
     * @UpdateUser: 更新者
     * @UpdateDate: 2020/11/30 11:49
     * @UpdateRemark: 更新说明
     * @Params:
     * @Retun: object/any
     */
    fun putConflictnum(): Int {
        // 获取之前的冲突数量
        var num = getConflictNUm()
        //每一次冲突加一
        num += 1
        mmkvspace.encode("conflictnum", num)
        return num
    }

    /**
     *
     * @Description: 获取总数
     * @Author: 魏征
     * @CreateDate: 2020/11/30 11:51
     * @UpdateUser: 更新者
     * @UpdateDate: 2020/11/30 11:51
     * @UpdateRemark: 更新说明
     * @Params:
     * @Retun: object/any
     */
    fun getConflictNUm(): Int {
        val num = mmkvspace.decodeInt("conflictnum", 0)
        return num
    }

    /**
     *
     * @Description: 删除
     * @Author: 魏征
     * @CreateDate: 2020/11/30 11:52
     * @UpdateUser: 更新者
     * @UpdateDate: 2020/11/30 11:52
     * @UpdateRemark: 更新说明
     * @Params:
     * @Retun: object/any
     */
    fun deleteConfilctNum() {
        mmkvspace.encode("conflictnum", 0)
    }


    /**
     *
     * @Description: 添加冲突数
     * @Author: 魏征
     * @CreateDate: 2020/11/30 11:49
     * @UpdateUser: 更新者
     * @UpdateDate: 2020/11/30 11:49
     * @UpdateRemark: 更新说明
     * @Params:
     * @Retun: object/any
     */
    fun putdengernum(): Int {
        // 获取之前的冲突数量
        var num = getdengerNUm()
        //每一次冲突加一
        num += 1
        mmkvspace.encode("dengernum", num)
        return num
    }

    /**
     *
     * @Description: 获取总数
     * @Author: 魏征
     * @CreateDate: 2020/11/30 11:51
     * @UpdateUser: 更新者
     * @UpdateDate: 2020/11/30 11:51
     * @UpdateRemark: 更新说明
     * @Params:
     * @Retun: object/any
     */
    fun getdengerNUm(): Int {
        val num = mmkvspace.decodeInt("dengernum", 0)
        return num
    }

    /**
     *
     * @Description: 删除
     * @Author: 魏征
     * @CreateDate: 2020/11/30 11:52
     * @UpdateUser: 更新者
     * @UpdateDate: 2020/11/30 11:52
     * @UpdateRemark: 更新说明
     * @Params:
     * @Retun: object/any
     */
    fun deletedengerNum() {
        mmkvspace.encode("dengernum", 0)
    }

//    /**
//     * 添加冲突
//     */
//    fun addCONFLICT(spaceCode: String, spaceId: String) {
//        //添加总数
//        val ConflictNUm = putConflictnum()
//        var setarry = mmkvspace.decodeInt(spaceId, 0)
//        setarry += 1
//
//
//        mmkvspace.encode(spaceId, setarry)
//
//
//
//
//        when (setarry) {
//            5 -> {
//                /**
//                 * 判断冲突数量大于5 判定这个泊位有问题 创建消息给收费看，并发送通知
//                 */
//
//                val dengerNUm = putdengernum()
//                CoroutineScope(Dispatchers.IO).launch {
//
//                    var spaceNum = 0
//                    MessageHelper.getInstance().parkDao?.getAll()?.size?.let {
//                        spaceNum = it
//                    }
//
//                    //  当次发送消息应是   故障占点  故障泊位号   冲突发生次数    本地一次记录存储    应一次发送给管理端，通知其检查  复位 或 停用   维修
//                    val json = Message.ConfilcontforMsg(spaceCode = spaceCode, spaceId = spaceId, congfilctnum = ConflictNUm, spaceNuml = spaceNum, dender = dengerNUm, site = ServiceFactory.getInstance().accountService.userDataNodb.siteName.toString())
//                    val msg_qian = Message(time = Date().time, type = Message.MsType.MALFUNCTION, message = Gson().toJson(json), account = ServiceFactory.getInstance().accountService.userDataNodb.name, localorder = spaceId, consumption = true)
//
//                    MessageHelper.getInstance().messageDao?.insertMsg(msg_qian)
//                    utilsUrl().sendBroadcast(msg_qian)
//                }
//
//
////发送有序广播
//
//            }
//            8 -> {
//                // val dengerNUm = putdengernum()
////                val json = Message.ConfilcontforMsg(spaceCode = spaceCode, spaceId = spaceId, congfilctnum = ConflictNUm, spaceNuml = 0, dender = getdengerNUm() , site = ServiceFactory.getInstance().accountService.userDataNodb.siteName.toString())
////                val msg_qian = Message(time = Date().time, type = Message.MsType.MALFUNCTION, message = Gson().toJson(json), account = ServiceFactory.getInstance().accountService.userDataNodb.name, localorder = spaceId, consumption = true)
////                MessageHelper.getInstance().InsertMsg(msg_qian)
//
//            }
//            0, 1, 2, 3, 4 -> {
//
//            }
//
//            else -> {
//
//                val alarmManager = currentActivity?.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager?
//
//                val triggerAtTime = System.currentTimeMillis() + 5 * 1000 //得到当前时间并且增加5秒，表示我们在5秒后触发定时器
//                val i = Intent("WEIZHENG_MSG")
//                val pIntent = PendingIntent.getBroadcast(currentActivity, 0, i, PendingIntent.FLAG_UPDATE_CURRENT)
//                alarmManager!![AlarmManager.RTC_WAKEUP, triggerAtTime] = pIntent
//            }
//        }
//
//
//    }


    /**
     * 获取冲突数量
     */
    fun getCONFLICTsize(spaceId: String): Int {
        return getCONFLICTlist(spaceId).size
    }

    /**
     * 获取冲突数量
     */
    fun getCONFLICTlist(spaceId: String): Set<String> {
        val setarry = mmkvspace.decodeStringSet(spaceId)
        return setarry
    }


    /**
     * 移除冲突
     */
    fun removeConflict(spaceId: String) {
        try {
            mmkvspace.remove(spaceId)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    /**
     * 清空分区中的类容
     */
    fun clean() {
        mmkvspace.clearAll()
    }


}

