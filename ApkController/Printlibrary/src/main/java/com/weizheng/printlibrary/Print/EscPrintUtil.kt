package com.weizheng.printlibrary.Print


import android.content.Context
import android.util.Log

import com.example.print_sdk.PrintUtil
import com.example.print_sdk.enums.ALIGN_MODE
import com.example.print_sdk.enums.MODE_ENLARGE
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


import java.io.IOException

/**
 *
 * @Description: 作用描述 一体化打印机方案
 * @Author: EscPrintUtil
 * @CreateDate: weizheng  10:14
 * @UpdateUser: 更新者
 * @UpdateDate: weizheng 10:14
 * @UpdateRemark: 更新说明
 * @Version: 1
 */
class EscPrintUtil : BasePrintAdapter {

    private object Helper {
        val instance = EscPrintUtil()
    }

    companion object {
        fun getInstance() = Helper.instance
    }

    var pUtil: PrintUtil? = null
    override fun initprinter(context: Context) {

    }

    override fun connet(address: String): Boolean {
        connectLister?.OnConnetState(ConnectState.START)
        if (pUtil == null) {
            initvoid()
        }
        connectLister?.OnConnetState(ConnectState.SUCCESS)
        return true
    }


    fun initvoid() {

        pUtil = PrintUtil.getClient()
        pUtil?.setPrintEventListener(object : PrintUtil.OnPrintEventListener {
            override fun onPrintStatus(state: Int) {

                when (state) {
                    0 -> {
                        //打印完成
                        connectstateListener?.doOnComplete()
                        number += 1
                        //    disconnet()
                    }
                    1 -> {
                        // 缺纸
                        connectLister?.OnConnetState(ConnectState.NO_PAGER)
                        //   disconnet()
                    }

                    2 -> {
                        //失败
                        connectstateListener?.doFailrue()
                        //  disconnet()
                    }


                }
            }

            override fun onVersion(p0: String?) {
                // Logger.e("esc version:$p0")
            }

        })

        pUtil?.setEncoding("GB2312")
        pUtil?.printEnableCertificate(true);
        pUtil?.printEnableMark(false);
        pUtil?.printLanguage(15);
        pUtil?.printEncode(3);
        pUtil?.getVersion();


    }

    override fun disconnet(): Boolean {

        connectLister = null
        connectstateListener = null

        return true

    }

    var number = 1000000001
    private fun printData(data: ArrayList<PrintModel>, pUtil: PrintUtil): Int {

        Log.i("printlibr", "esc print:开始打印")
        CoroutineScope(Dispatchers.IO).launch {
            try {
                pUtil.printState()
                pUtil.printStartNumber(number);
                pUtil.printConcentration(25);


                //  printText(number, pUtil)
                data.forEach {
                    try {
                        when (it.wordSize) {
                            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 -> {

                                pUtil.printFontSize(MODE_ENLARGE.NORMAL)

                            }
                            20, 21, 22 -> {
                                pUtil.printFontSize(MODE_ENLARGE.NORMAL)

                            }
                            23, 24, 25, 26 -> {
                                pUtil.printFontSize(MODE_ENLARGE.NORMAL)

                            }
                            27, 28, 29 -> {
                                pUtil.printFontSize(MODE_ENLARGE.HEIGHT_DOUBLE)


                            }
                            else -> {
                                pUtil.printFontSize(MODE_ENLARGE.WIDTH_DOUBLE)
                                //   pUtil.printFontSize(MODE_ENLARGE.HEIGHT_WIDTH_DOUBLE)
                            }
                        }
                        when (it.type) {
                            1 -> {
                                when (it.location) {
                                    1 -> {
                                        pUtil.printAlignment(ALIGN_MODE.ALIGN_LEFT)
                                    }
                                    2 -> {
                                        pUtil.printAlignment(ALIGN_MODE.ALIGN_CENTER)
                                    }
                                    3 -> {
                                        pUtil.printAlignment(ALIGN_MODE.ALIGN_RIGHT)
                                    }
                                }
                                if (it.bold == 1) {
                                    pUtil.printTextBold(true)
                                } else {
                                    pUtil.printTextBold(false)
                                }

                                pUtil.printText(it.content)

                                pUtil.printText("\n")
                            }
                            2 -> {
                                var currentWidth: Int = 0
                                when (it.location) {
                                    1 -> {
                                        pUtil.printAlignment(ALIGN_MODE.ALIGN_LEFT)
                                    }
                                    // currentWidth = 0
                                    2 -> {
                                        pUtil.printAlignment(ALIGN_MODE.ALIGN_CENTER)
                                        currentWidth = 280
                                    }


                                    3 -> {
                                        pUtil.printAlignment(ALIGN_MODE.ALIGN_RIGHT)
                                        currentWidth = 280
                                    }

                                }

                                if (it.bold == 1) {
                                    pUtil.printTextBold(true)
                                } else {
                                    pUtil.printTextBold(true)
                                }
                                pUtil.printQR(it.content, currentWidth, currentWidth)
                            }
                            3 -> {
                                when (it.location) {
                                    1 -> {
                                        pUtil.printAlignment(ALIGN_MODE.ALIGN_LEFT)
                                    }
                                    2 -> {
                                        pUtil.printAlignment(ALIGN_MODE.ALIGN_CENTER)

                                    }
                                    3 -> {
                                        pUtil.printAlignment(ALIGN_MODE.ALIGN_RIGHT)
                                    }

                                }

                                if (it.bold == 1) {
                                    pUtil.printTextBold(true)
                                } else {
                                    pUtil.printTextBold(false)
                                }


                                pUtil.printDashLine()
                                pUtil.printText("\n")
                            }

                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                pUtil.printText("\n")
                pUtil.printText("\n")
                pUtil.printText("\n")
                pUtil.printEndNumber()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return 0

    }


    fun printText(number: Int, pUtil: PrintUtil) {

        try {
            pUtil.printState()
            pUtil.printStartNumber(number)
            pUtil.printConcentration(25)
            pUtil.printFontSize(MODE_ENLARGE.NORMAL)
            pUtil.printTextBold(true) // 是否加粗
            pUtil.printAlignment(ALIGN_MODE.ALIGN_LEFT) // 对齐方式
            pUtil.printText(textZhContent())
            pUtil.printTextBold(false) // 关闭加粗
            pUtil.printFontSize(MODE_ENLARGE.NORMAL) // 字体大小
            pUtil.printLine()
            pUtil.printDashLine()
            pUtil.printLine(2)
            pUtil.printAlignment(ALIGN_MODE.ALIGN_CENTER)
            pUtil.printBarcode("123456", 80, 2)
            pUtil.printLine()
            pUtil.printQR("1234456", 200, 200)
            pUtil.printLine(2)
            pUtil.printEndNumber()
        } catch (e: IOException) {
        }
    }


    fun textZhContent(): String? {
        val sb = StringBuilder()
        sb.append("        收 银 凭 据            ")
        sb.append("\n")
        sb.append("时间   : ")
        sb.append("2016-11-15     16:00")
        sb.append("\n")
        sb.append("操作员:admin")
        sb.append("\n")
        sb.append("收据单号：1234567890")
        sb.append("\n")
        sb.append("编号  数量  单价  折扣  小计")
        sb.append("\n")
        /*
			sb.append("下划线——————————————————\n");

			sb.append("横线--------------------\n");
			sb.append("横线---------下划线————————\n");
			sb.append("------------------------\n");
			sb.append("——————————————————————————\n");
			sb.append("=== === === = = =   =======\n");
			sb.append("+++++++----————............\n");
			sb.append(".........................\n");
			sb.append(".|||||//////||||||\n");
            */sb.append("-----------------------------")
        sb.append("\n")
        sb.append("AM126   1  1200  0   1200")
        sb.append("\n")
        sb.append("AM127   1  1300  0   1300")
        sb.append("\n")
        sb.append("AM128   1  1400  0   1400")
        sb.append("\n")
        sb.append("-----------------------------")
        sb.append("\n")
        sb.append("共销售数量: 3 ")
        sb.append("\n")
        sb.append("售价合计(RMB): 3900")
        sb.append("\n")
        sb.append("实收金额(RMB): 3900")
        sb.append("\n")
        sb.append("找零金额(RMB): 0")
        sb.append("\n")
        sb.append("-----------------------------")
        sb.append("\n")
        sb.append("支付方式: 微信支付 ")
        sb.append("\n")
        sb.append("\n")
        sb.append("\n")
        return sb.toString()
    }


    override fun PrintData(data: ArrayList<PrintModel>) {


        connectstateListener?.doStart()
        printData(data, pUtil!!)

    }


    var connectLister: OnPrintconnetListener? = null
    var connectstateListener: OnPrintingStateListener? = null
    override fun addConnectListener(listener: OnPrintconnetListener) {
        connectLister = listener
    }

    override fun addConnectingListener(listener: OnPrintingStateListener) {
        connectstateListener = listener
    }


}