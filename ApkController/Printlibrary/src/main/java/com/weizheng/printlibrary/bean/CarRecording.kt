package com.weizheng.printlibrary.bean

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.SerializedName


/**
 * time 发生时间
 * cartype  停车类型
 * plateNumber 车牌
 * id 停车记录id
 * intime 进场时间
 * outtime 出场时间
 * pic 进场照片
 * spce_id 泊位id
 * inManName 进车操作人员名称
 * outManName 出车操作人元名称
 * spce_code 车位编号
 * prepay 预缴金额
 * usertype 用户类型
 * fee  停车产生费用
 * actual_payment 实际付款
 * outcar_type 出车操作类型
 * cpAccount	Float	微信企业账户余额
 * psAccount	Float	微信充值用户个人账户余额
 * wcRecFee	Float	微信缴费出车缴费金额
 * refund    Boolean 是否需要退款
 * isPlay   是否通过二维码扫码支付成功！ 支付成功显示  已支付多少元，和出车按钮
 * playfee  已支付金额 不包含 预缴金额  change 修改为
 * orderNo [] ARRAY订单号
 */


data class CarRecording(


    var time: Long,

    var cartype: Int,

    var plateNumber: String,

    var id: Long,

    @SerializedName("inTime")
    var intime: String,

    @SerializedName("outTime")
    var outtime: String? = null,

    var piclist: List<String>,
    @SerializedName("pic")
    val picstring: String? = "",

    var spce_id: String,

    var inManName: String,


    var outManName: String? = null,

    @SerializedName("lotCode")
    var spce_code: String,
    var parkName: String,

    @SerializedName("preFee")
    var prepay: Float? = 0f,

    @SerializedName("carType")
    var usertype: Int? = 0,
    var user_type: String? = "",

    var fee: Float? = 0.0F,

    var actual_payment: String? = null,
    var recFee: Float? = 0f,
    var chRule: String? = "",

    var outcar_type: Int = 0,
    var parkingTime: String? = "",
    var cpAccount: Float = 0.0F,
    var psAccount: Float = 0.0F,
    var wcRecFee: Float = 0.0F,
    var refund: Boolean = false,
    var isPlay: Boolean = false,
    var playfee: Float = 0.0F,
    var orderNo: List<String>,
    var inManPhoneNo: String = "",
    var outManPhoneNo: String = "",
    var arrfee: Float = 0.0F,
    var wg: Boolean = false
) : Parcelable {


    var parking_id: Long = 0

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readLong(),
        parcel.readString().toString(),
        parcel.readString(),
        parcel.createStringArrayList()!!,
        parcel.readString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readValue(Float::class.java.classLoader) as? Float,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Float::class.java.classLoader) as? Float,
        parcel.readString(),
        parcel.readValue(Float::class.java.classLoader) as? Float,
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readFloat(),
        parcel.readFloat(),
        parcel.readFloat(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readFloat(),
        parcel.createStringArrayList()!!,
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readFloat(),
        parcel.readByte() != 0.toByte()
    ) {
        parking_id = parcel.readLong()

    }

    class UserType {
        companion object {
            const val NONE = 0
            const val TEMP = 1//临停用户
            const val WECHAT_COMPANY_USER = 2//微信企业
            const val CHARE_USER = 3//微信充值
            const val MITILTARY_USER = 4//军警车
            const val ELECTRONIC_USER = 5//新能源车

            // const val WECHAT_COMMON = 6//内部车
            const val WECHAT_PAY_USER = 7//微信用户缴费
            const val wuganplay = 6 //无感支付用户
        }

    }

    /*
    *
    *  when (chType) {
                UserType.TEMP -> {
                    model = PrintModel(1, "停车类型：临停", 1, 20, 10)
                    list.add(model)
                }

                UserType.WECHAT_COMPANY_USER -> {
                    model = PrintModel(1, "停车类型：微信企业", 1, 20, 10)
                    list.add(model)
                }
                UserType.WECHAT_CHARGE_USER -> {
                    model = PrintModel(1, "停车类型：微信充值", 1, 20, 10)
                    list.add(model)
                }
                UserType.WECHAT_PAY_USER -> {
                    model = PrintModel(1, "停车类型：微信用户缴费", 1, 20, 10)
                    list.add(model)
                }
                UserType.ELECTRONIC_USER -> {
                    model = PrintModel(1, "停车类型：新能源车", 1, 20, 10)
                    list.add(model)
                }
            }

    *
    * */
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(time)
        parcel.writeInt(cartype)
        parcel.writeString(plateNumber)
        parcel.writeLong(id)
        parcel.writeString(intime)
        parcel.writeString(outtime)
        parcel.writeStringList(piclist)
        parcel.writeString(picstring)
        parcel.writeString(spce_id)
        parcel.writeString(inManName)
        parcel.writeString(outManName)
        parcel.writeString(spce_code)
        parcel.writeString(parkName)
        parcel.writeValue(prepay)
        parcel.writeValue(usertype)
        parcel.writeString(user_type)
        parcel.writeValue(fee)
        parcel.writeString(actual_payment)
        parcel.writeValue(recFee)
        parcel.writeString(chRule)
        parcel.writeInt(outcar_type)
        parcel.writeString(parkingTime)
        parcel.writeFloat(cpAccount)
        parcel.writeFloat(psAccount)
        parcel.writeFloat(wcRecFee)
        parcel.writeByte(if (refund) 1 else 0)
        parcel.writeByte(if (isPlay) 1 else 0)
        parcel.writeFloat(playfee)
        parcel.writeLong(parking_id)
        parcel.writeStringList(orderNo)
        parcel.writeString(inManPhoneNo)
        parcel.writeString(outManPhoneNo)
        parcel.writeFloat(arrfee)
        parcel.writeByte(if (wg) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CarRecording> {
        override fun createFromParcel(parcel: Parcel): CarRecording {
            return CarRecording(parcel)
        }

        override fun newArray(size: Int): Array<CarRecording?> {
            return arrayOfNulls(size)
        }
    }
}