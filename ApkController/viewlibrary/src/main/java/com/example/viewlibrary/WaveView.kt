package com.example.viewlibrary

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class WaveView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet,
    defStyleAttr: Int
) : View(
    context,
    attributeSet,
    defStyleAttr
) {

    /*画笔颜色*/
    private var _paintcolor = Color.RED

    /*画笔宽度*/
    private var _paintwidth = 20

    /*画笔类型Flag*/
    private var _paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        _paint.color = _paintcolor
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        /*计算xml中设计的宽高大小*/
        var widthSpecSize = View.MeasureSpec.getSize(widthMeasureSpec)
        var heightSpecSize = View.MeasureSpec.getSize(heightMeasureSpec)
        var mlayoutSize = Math.min(widthSpecSize, heightSpecSize)
       setMeasuredDimension(mlayoutSize,mlayoutSize)


    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)


    }

}