package com.example.viewlibrary

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlin.math.abs


/**
 *
 * @Description: 作用描述
 * @Author: BaseFullBottomSheetFragment
 * @CreateDate: 1  9:57
 * @UpdateUser: 更新者
 * @UpdateDate: 1 9:57
 * @UpdateRemark: 更新说明
 * @Version:
 */
class BaseFullBottomSheetFragment() : BottomSheetDialogFragment() {


    val TAG = BaseFullBottomSheetFragment::class.java.simpleName

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        Log.e(TAG, "onCreateDialog")
        return BottomSheetDialog(this.requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.window_layout, container, false)


        return view
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onStart() {
        Log.i(TAG, "ONStart")

        //获取dialog 对象
        val dialog = dialog as BottomSheetDialog
        //把windowsd的默认背景颜色去掉，不然圆角显示不见
        dialog.window!!.findViewById<FrameLayout>(R.id.design_bottom_sheet).setBackgroundDrawable(
            ColorDrawable(
                Color.TRANSPARENT
            )
        )

        //获取diglog的根部局
        val bottomSheet = dialog.delegate.findViewById<FrameLayout>(R.id.design_bottom_sheet)
        if (bottomSheet != null) {
            val layoutParams = bottomSheet.layoutParams as CoordinatorLayout.LayoutParams
            layoutParams.height = getPeekHeight()
            //修改弹窗的最大高度，不允许上滑（默认可以上滑）
            bottomSheet.layoutParams = layoutParams;
            val behavior = BottomSheetBehavior.from(bottomSheet)
            //peekHeight即弹窗的最大高度
            behavior.peekHeight = resources.displayMetrics.heightPixels;
            // 初始为展开状态
            behavior.state = BottomSheetBehavior.STATE_EXPANDED;
//            val mReBack: ImageView = view!!.findViewById(R.id.re_back_img)


            var PointerY = 0f
            var nextPointerY = 0f

            bottomSheet.setOnTouchListener(View.OnTouchListener { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        PointerY = event.y
                    }

                    MotionEvent.ACTION_MOVE -> {
                        nextPointerY = event.y

                        //屏幕高度减去marinTop作为控件的Height
                        layoutParams.height =
                            (layoutParams.height.toFloat() + (PointerY - nextPointerY)).toInt()
                        bottomSheet.layoutParams = layoutParams

                        Log.e("base", "$nextPointerY :  $PointerY")
                    }


                    MotionEvent.ACTION_UP -> {
                        //
                        Log.e(
                            "base",
                            "height:${layoutParams.height}  限高：${getPeekHeight() + getPeekHeightof()} "
                        )

                        Log.e("base", "$nextPointerY :  $PointerY")
                        //向下
                        if (abs(nextPointerY - PointerY) > getPeekHeightof()) {
                             //大于0 向上
                            if (nextPointerY - PointerY > 0) {
                                layoutParams.height = resources.displayMetrics.heightPixels
                                bottomSheet.layoutParams = layoutParams
                            }else{
                                layoutParams.height = getPeekHeight()
                                bottomSheet.layoutParams = layoutParams
                            }

                        }
                        nextPointerY = 0f

                    }
                }

                true
            })
        }

        super.onStart()
    }


    /**
     * 弹窗高度，默认为屏幕高度的四分之三
     * 子类可重写该方法返回peekHeight
     *
     * @return height
     */
    private fun getPeekHeight(): Int {
        val peekHeight = resources.displayMetrics.heightPixels;
        //设置弹窗高度为屏幕高度的3/4
        return peekHeight - peekHeight / 3;
    }


    /**
     * 弹窗高度，默认为屏幕高度的四分之三
     * 子类可重写该方法返回peekHeight
     *
     * @return height
     */
    private fun getPeekHeightof(): Int {
        val peekHeight = resources.displayMetrics.heightPixels;
        //设置弹窗高度为屏幕高度的3/4
        return peekHeight / 5;
    }

}